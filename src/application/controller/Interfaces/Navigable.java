package application.controller.Interfaces;

import application.controller.menu_controllers.MainMenuController;

public interface Navigable {
	public String getErrorMessage(); 
	public void setParentController(MainMenuController mainController); 
}
