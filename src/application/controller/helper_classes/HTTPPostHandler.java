package application.controller.helper_classes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import application.model.User;

public abstract class HTTPPostHandler {
	private final String URL_STRING = "http://nosuit.io/upload.php";
	private final String DQ_FILE_PATH_STRING = "temp_files/pdf_forms/dq_file.pdf";
	protected User currentUser;

	public abstract void handleSuccessCase();
	public abstract void handleFailureCase();

	public void uploadForm() throws Exception{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		File file = new File(DQ_FILE_PATH_STRING);

		HttpEntity entity = MultipartEntityBuilder
			    .create()
			    .addTextBody("companyName", currentUser.getCompany().getName())
			    .addTextBody("employeeName", currentUser.getPersonalInformation().getName())
			    .addBinaryBody("fileToUpload", file, ContentType.create("application/octet-stream"),file.getName())
			    .build();

		HttpPost httpPost = new HttpPost(URL_STRING);
		httpPost.setEntity(entity);
		HttpResponse response = httpclient.execute(httpPost);
		String json_string = EntityUtils.toString(response.getEntity());

		System.out.println(json_string.toString());

	}
}
