package application.controller.helper_classes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONException;

public class JSONLoader {
	private String path; 
	
	public JSONLoader(String path){
		this.path = path; 
	}
	
	public JSONArray generateJSONArray(){
		JSONArray jsonArray = null;
		try {
			jsonArray = new JSONArray(getJSONString());
		} catch (JSONException e) {
			System.out.println(e.toString());
		}
	
		return jsonArray; 
	}
	
	private String getJSONString(){
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path)));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();
	}

}
