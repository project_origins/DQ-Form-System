package application.controller.helper_classes;

import java.util.function.UnaryOperator;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;

public class TextFormatterSingleton {
    private static TextFormatterSingleton instance = null;
   
    protected TextFormatterSingleton() {
       // Exists only to defeat instantiation.
    }
   
    public static TextFormatterSingleton getInstance() {
       if(instance == null) {
          instance = new TextFormatterSingleton();
       }
       return instance;
    }
	
	public UnaryOperator<Change> getDoubleFilter(){
		return new UnaryOperator<TextFormatter.Change>() {
	        @Override
	        public TextFormatter.Change apply(TextFormatter.Change t) {
	            if (t.isReplaced()) 
	                if(t.getText().matches("[^0-9]"))
	                    t.setText(t.getControlText().substring(t.getRangeStart(), t.getRangeEnd()));
	            
	            if (t.isAdded()) {
	                if (t.getControlText().contains(".")) {
	                    if (t.getText().matches("[^0-9]")) {
	                        t.setText("");
	                    }
	                } else if (t.getText().matches("[^0-9.]")) {
	                    t.setText("");
	                }
	            }
	            return t;
	        }
	    };
	}
	
	public void setIntegerFilter(TextField textField){
		textField.textProperty().addListener(new ChangeListener<String>() {
  	        @Override
  	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
  	            if (!newValue.matches("\\d*")) {
  	                textField.setText(newValue.replaceAll("[^\\d]", ""));
  	            }
  	        }
  	    });
	}
}
