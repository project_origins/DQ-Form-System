package application.controller.menu_controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.controller.Interfaces.Navigable;
import application.controller.helper_classes.HTTPPostHandler;
import application.controller.helper_classes.PDFWriter;
import application.controller.screen_controllers.AddressInformationController;
import application.controller.screen_controllers.CompletionController;
import application.controller.screen_controllers.CriminalInformationController;
import application.controller.screen_controllers.EducationInformationController;
import application.controller.screen_controllers.FileUploadController;
import application.controller.screen_controllers.FormSelectionController;
import application.controller.screen_controllers.HistoryOfEmploymentController;
import application.controller.screen_controllers.HoursOfServiceController;
import application.controller.screen_controllers.ReviewController;
import application.controller.screen_controllers.StartController;
import application.controller.screen_controllers.UploadController;
import application.controller.screen_controllers.UserInformationController;
import application.model.User;
import application.model.enums.Page;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

public class MainMenuController extends HTTPPostHandler implements Initializable{
	final private String SCREEN_PATH = "/resources/fxml/screens";

	private boolean isTesting = false; //TODO: To be deleted, for testing purposes only

	private Page currentPage;

	private PDFWriter pdfWriter;

	private HBox startScreen;
	private HBox formSelectionScreen;
	private AnchorPane fileUploadScreen;
	private HBox userInformationScreen;
	private HBox addressInformationScreen;
	private HBox historyOfEmploymentInformationScreen;
	private HBox educationInformationScreen;
	private HBox criminalInformationScreen;
	private HBox hoursOfServiceScreen;
	private HBox reviewScreen;
	private HBox uploadScreen;
	private HBox completionScreen;

	private StartController startController;
	private UserInformationController personalInformationController;
	private FormSelectionController formSelectionController;
	private AddressInformationController addressInformationController;
	private HistoryOfEmploymentController historyOfEmploymentController;
	private EducationInformationController educationInformationController;
	private CriminalInformationController criminalInformationController;
	private HoursOfServiceController hoursOfServiceController;
	private UploadController uploadController;
	private FileUploadController fileUploadController;
	private ReviewController reviewController;
	private CompletionController completionController;

    @FXML private HBox mainPane;
    @FXML private Label errorMessage;

    @FXML private Button forwardButton;
    @FXML private Button clearButton;
    @FXML private Button backButton;

    @FXML private StackPane dialogOverlay;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		currentUser = new User();
		pdfWriter = new PDFWriter();

		currentPage = Page.START;
		initializeScreens();

		mainPane.getChildren().add(startScreen);

		if(isTesting)
			setTestData();

		hideButtons();
	}

	public void setTestData(){
		formSelectionController.setTestData();
		showButtons();
		personalInformationController.setTestData();
		addressInformationController.setTestData();
		historyOfEmploymentController.setTestData();
		educationInformationController.setTestData();
		criminalInformationController.setTestData();
		hoursOfServiceController.setTestData();
	}

	public FXMLLoader generateLoader(String path){
		return new FXMLLoader(getClass().getResource(path));
	}

	private void initializeScreens(){
		try {
			FXMLLoader startLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/StartScreen.fxml"));
			startScreen = startLoader.load();
			startController = startLoader.<StartController>getController();
			((Navigable) startController).setParentController(this);

			FXMLLoader formLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/FormSelectionScreen.fxml"));
			formSelectionScreen = formLoader.load();
			formSelectionController = formLoader.<FormSelectionController>getController();
			((Navigable) formSelectionController).setParentController(this);

			FXMLLoader fileUploadLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/FileUploadScreen.fxml"));
			fileUploadScreen = fileUploadLoader.load();
			fileUploadController = fileUploadLoader.<FileUploadController>getController();
			((Navigable) fileUploadController).setParentController(this);

			FXMLLoader userInformationLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/UserInformationScreen.fxml"));
			userInformationScreen = userInformationLoader.load();
			personalInformationController = userInformationLoader.<UserInformationController>getController();

			FXMLLoader addressLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/AddressInformationScreen.fxml"));
			addressInformationScreen = addressLoader.load();
			addressInformationController = addressLoader.<AddressInformationController>getController();
			((Navigable) addressInformationController).setParentController(this);

			FXMLLoader historyOfEmploymentLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/HistoryOfEmploymentScreen.fxml"));
			historyOfEmploymentInformationScreen = historyOfEmploymentLoader.load();
			historyOfEmploymentController = historyOfEmploymentLoader.<HistoryOfEmploymentController>getController();
			((Navigable) historyOfEmploymentController).setParentController(this);

			FXMLLoader educationInformationLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/EducationInformationScreen.fxml"));
			educationInformationScreen = educationInformationLoader.load();
			educationInformationController = educationInformationLoader.<EducationInformationController>getController();
			((Navigable) educationInformationController).setParentController(this);

			FXMLLoader criminalInformationLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/CriminalInformationScreen.fxml"));
			criminalInformationScreen = criminalInformationLoader.load();
			criminalInformationController = criminalInformationLoader.<CriminalInformationController>getController();
			((Navigable) criminalInformationController).setParentController(this);

			FXMLLoader hoursOfServiceLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/HoursOfServiceScreen.fxml"));
			hoursOfServiceScreen = hoursOfServiceLoader.load();
			hoursOfServiceController = hoursOfServiceLoader.<HoursOfServiceController>getController();
			((Navigable) hoursOfServiceController).setParentController(this);

			FXMLLoader reviewLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/ReviewScreen.fxml"));
			reviewScreen = reviewLoader.load();
			reviewController = reviewLoader.<ReviewController>getController();
			((Navigable) reviewController).setParentController(this);

			FXMLLoader uploadScreenLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/UploadScreen.fxml"));
			uploadScreen = uploadScreenLoader.load();
			uploadController = uploadScreenLoader.<UploadController>getController();
			((Navigable) uploadController).setParentController(this);

			FXMLLoader completionLoader = new FXMLLoader(getClass().getResource(SCREEN_PATH + "/CompletionScreen.fxml"));
			completionScreen = completionLoader.load();
			completionController = completionLoader.<CompletionController>getController();
			((Navigable) completionController).setParentController(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    @FXML
    public void showPreviousScreen(ActionEvent event) {
    	switch(currentPage){
    		case FORM_SELECTION:
    			currentPage = Page.START;
    			setCurrentPage(startScreen);
    			hideButtons();
    			break;
	    	case FILE_UPLOAD:
	    		currentPage = Page.FORM_SELECTION;
	    		setCurrentPage(formSelectionScreen);
	    		backButton.setText("Exit");
	    		break;
	    	case IDENTIFICAITON_INFORMATION:
	    		currentPage = Page.FILE_UPLOAD;
	    		setCurrentPage(fileUploadScreen);
	    		break;
	    	case ADDRESS:
	    		currentPage = Page.IDENTIFICAITON_INFORMATION;
	    		setCurrentPage(userInformationScreen);
	    		break;
	    	case EMPLOYMENT:
	    		currentPage = Page.ADDRESS;
	    		setCurrentPage(addressInformationScreen);
	    		break;
	    	case EDUCATION:
	    		currentPage = Page.EMPLOYMENT;
	    		setCurrentPage(historyOfEmploymentInformationScreen);
	    		break;
	    	case CRIME:
	    		currentPage = Page.EDUCATION;
	    		setCurrentPage(educationInformationScreen);
	    		break;
	    	case REVIEW:
	    		currentPage = Page.CRIME;
	    		setCurrentPage(criminalInformationScreen);
	    		forwardButton.setText("Next");
	    		break;
	    	default:
	    		break;
		}
    }

    @FXML
    public void clearCurrentScreen(ActionEvent event) {
    	switch(currentPage){
	    	case FILE_UPLOAD:
	    		break;
	    	case IDENTIFICAITON_INFORMATION:
	    		personalInformationController.clearFields();
	    		break;
	    	case ADDRESS:
	    		addressInformationController.clearFields();
	    		break;
	    	case EMPLOYMENT:
	    		historyOfEmploymentController.clearFields();
	    		break;
	    	case EDUCATION:
	    		educationInformationController.clearFields();
	    		break;
	    	case CRIME:
	    		criminalInformationController.clearFields();
	    		break;
	    	case REVIEW:
	    		break;
	    	default:
	    		break;
		}
    }

    public void startDQFile(boolean isExistingDQFile) {
    		if (isExistingDQFile) {
    			//TODO: implement loading DQ file from memory
    		}
    		showNextScreen(null);
    }

    @FXML
    public void showNextScreen(ActionEvent event) {
    	showErrorMessage("");
		switch(currentPage){
		case START:
			currentPage = Page.FORM_SELECTION;
			setCurrentPage(formSelectionScreen);
			backButton.setText("Exit");
			break;
	    	case FORM_SELECTION:
	    		if(((Navigable)formSelectionController).getErrorMessage().equals("valid")){
	    			currentUser.setCompanyApplyingForInformation(formSelectionController.getSelectedCompany());
	    			currentPage = Page.FILE_UPLOAD;
	    			setCurrentPage(fileUploadScreen);
	    			backButton.setText("Back");
	    		}else
	    			showErrorMessage(((Navigable)formSelectionController).getErrorMessage());
	    		break;
	    	case FILE_UPLOAD:
	    		if(((Navigable)fileUploadController).getErrorMessage().equals("valid")){
	    			currentUser.setDLFrontImages(fileUploadController.getFrontOfLicense(), fileUploadController.getFrontOfLicenseAbsolutePath());
	    			currentUser.setDLBackImage(fileUploadController.getBackOfLicense(), fileUploadController.getBackOfLicenseAbsolutePath());

	    			currentPage = Page.IDENTIFICAITON_INFORMATION;
	    			setCurrentPage(userInformationScreen);
	    		}else
	    			showErrorMessage(((Navigable)fileUploadController).getErrorMessage());
	    		break;
	    	case IDENTIFICAITON_INFORMATION:
	    		if(((Navigable)personalInformationController).getErrorMessage().equals("valid")){
    	    		currentUser.setPersonalInformation(personalInformationController.generatePersonalInformationData());
    	    		currentUser.setPositionInformation(personalInformationController.generateEmploymentInformationData(), null);
    	    		currentUser.setRelativeList(personalInformationController.generateRelativeList());
    	    		currentUser.setEmergencyContacts(personalInformationController.generateEmergencyContactList());
    	    		currentUser.setDLInformation(
    	    				personalInformationController.generateDLInfoObject().getdlNumber(),
    	    				personalInformationController.generateDLInfoObject().getdlState(),
    	    				personalInformationController.generateDLInfoObject().getdlExpirationDate()
    	    				);

    	    		currentPage = Page.ADDRESS;
    	    		setCurrentPage(addressInformationScreen);
	    		}else
	    			showErrorMessage(((Navigable)personalInformationController).getErrorMessage());
	    		break;
	    	case ADDRESS:
	    		if(((Navigable)addressInformationController).getErrorMessage().equals("valid")){
	    			currentUser.setUserCurrentAddress(addressInformationController.getCurrentAddress());
	    			currentUser.setUserPreviousAddresses(addressInformationController.getPreviousAddressList());

    	    		currentPage = Page.EMPLOYMENT;
    	    		setCurrentPage(historyOfEmploymentInformationScreen);
	    		}else
	    			showErrorMessage(((Navigable)addressInformationController).getErrorMessage());
	    		break;
	    	case EMPLOYMENT:
	    		if(((Navigable)historyOfEmploymentController).getErrorMessage().equals("valid")){
	    			currentUser.setEmploymentHistory(historyOfEmploymentController.getEmploymentHistory());

	    			currentPage = Page.EDUCATION;
	    			setCurrentPage(educationInformationScreen);
	    		}else
	    			showErrorMessage(((Navigable)historyOfEmploymentController).getErrorMessage());
	    		break;
	    	case EDUCATION:
	    		if(((Navigable)educationInformationController).getErrorMessage().equals("valid")){
	    			currentUser.setEducation(educationInformationController.getEducation());

	    			currentPage = Page.CRIME;
	    			setCurrentPage(criminalInformationScreen);
	    		}else
	    			showErrorMessage(((Navigable)educationInformationController).getErrorMessage());
	    		break;
	    	case CRIME:
	    		if(((Navigable)criminalInformationController).getErrorMessage().equals("valid")){
	    			currentUser.setCriminalHistory(criminalInformationController.generateCriminalHistoryObject());
	    			currentUser.setBondDenied(criminalInformationController.isBondDenied());

	    			currentUser.setBondDeniedReason(criminalInformationController.isBondDenied() ? criminalInformationController.getBondReason() : "N/A");

	    			currentUser.setViolation(criminalInformationController.hasViolation());
	    			currentUser.setViolationReason(criminalInformationController.getViolationDescription());

	    			currentPage = Page.HOURS;
	    			setCurrentPage(hoursOfServiceScreen);
	    		}else
	    			showErrorMessage(((Navigable)criminalInformationController).getErrorMessage());
	    		break;
	    	case HOURS:
	    		if(((Navigable)hoursOfServiceController).getErrorMessage().equals("valid")) {
	    			currentUser.setHourseOfService(hoursOfServiceController.generateHoursOfServiceObject());

	    			currentPage = Page.REVIEW;
	    			forwardButton.setText("Finish");
	    			clearButton.setVisible(false);

	    			dialogOverlay.setVisible(true);
	    			initPDFTask();
	    		}else
	    			showErrorMessage(((Navigable)hoursOfServiceController).getErrorMessage());
	    		break;
	    	case REVIEW:
		    		currentPage = Page.UPLOAD;
		    		setCurrentPage(uploadScreen);
		    		hideButtons();
		    		dialogOverlay.setVisible(true);
	    			initDocumentStoreTask();
	    		break;
	    	case UPLOAD:
	    		currentPage = Page.COMPLETION;
	    		setCurrentPage(completionScreen);
	    		break;
	    	case COMPLETION:
	    		currentPage = Page.START;
	    		setCurrentPage(startScreen);
	    	default:
	    		break;
		}
    }

    private void initPDFTask() {
    		Task <Void> task = new Task<Void>() {
    			@Override
    			public Void call() throws InterruptedException {
    				writePDF();
				return null;
    			}

    			@Override
  	        protected void succeeded() {
  	            super.succeeded();
  	            updateMessage("Done!");
  	            reviewController.setPDFPath();
  	            currentPage = Page.REVIEW;
  	            dialogOverlay.setVisible(false);
  	            setCurrentPage(reviewScreen);
  	        }
    	    };

    	    Thread thread = new Thread(task);
    	    thread.setDaemon(true);
    	    thread.start();
    }

    private void initDocumentStoreTask() {
		Task <Void> task = new Task<Void>() {
			@Override
			public Void call() throws InterruptedException {
				try {
					uploadForm();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}

			@Override
	        protected void succeeded() {
	            super.succeeded();
	            updateMessage("Done!");
	            currentPage = Page.COMPLETION;
	            dialogOverlay.setVisible(false);
	    			setCurrentPage(completionScreen);
	        }
	    };

	    Thread thread = new Thread(task);
	    thread.setDaemon(true);
	    thread.start();
    }

    private void writePDF() {
     	pdfWriter.setUser(currentUser);
	    pdfWriter.write();
    }

    private void setCurrentPage(Node node){
		mainPane.getChildren().clear();
		mainPane.getChildren().add(node);
    }

    public void showErrorMessage(String errorMessageString){
    		errorMessage.setText(errorMessageString);
		errorMessage.setVisible(true);
    }

    public void hideButtons() {
		backButton.setVisible(false);
		clearButton.setVisible(false);
		forwardButton.setVisible(false);
    }

    public void showButtons() {
		backButton.setVisible(true);
		clearButton.setVisible(true);
		forwardButton.setVisible(true);
    }

    public User getUser(){
    		return currentUser;
    }

	@Override
	public void handleSuccessCase() {

	}

	@Override
	public void handleFailureCase() {
		// TODO Auto-generated method stub

	}

	//TODO: Delete - For testing purposes only
	public void setUser(User user){
		this.currentUser = user;
	}
}
