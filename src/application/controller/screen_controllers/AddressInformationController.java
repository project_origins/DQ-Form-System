package application.controller.screen_controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import application.controller.Interfaces.Clearable;
import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import application.controller.ui_component_controllers.PersonalAddressController;
import application.model.Address;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.VBox;

public class AddressInformationController implements Initializable, Navigable, Clearable{
	private MainMenuController mainController; 
	
	@FXML private PersonalAddressController currentAddressController;
	
	@FXML private VBox previousAddressContainer; 
	private ArrayList<PersonalAddressController> previousAddressControllerList; 

  	@Override
  	public void initialize(URL location, ResourceBundle resources) {
  		previousAddressControllerList = new ArrayList<PersonalAddressController>(); 
  	}
  	
  	public void setTestData(){
  		currentAddressController.setTestData(); 
  		
  		addPreviousAddress(null); 
  		previousAddressControllerList.get(0).setTestData(); 
  	}
  	
  	@FXML
    void addPreviousAddress(ActionEvent event) {
  		if(getTotalNumberOfYears() >= 3){
  			mainController.showErrorMessage("You have already added 3 years of your living information thank you.");
  		} else{
	  		FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/PersonalAddress.fxml"));
	   	 	try {
	   	 		
	   	 		previousAddressContainer.getChildren().add(loader.load());
	   	 		PersonalAddressController tempController = (PersonalAddressController) loader.getController(); 
	   	 		tempController.setParent(this);
	   	 		tempController.showDeleteButton();
	   	 		previousAddressControllerList.add(tempController); 
			} catch (IOException e) {
				System.out.println(e.toString());
			} 
  		}
    }
  	
	public Address getCurrentAddress(){
		return new Address(
			currentAddressController.getPhysicalAddress(), 
			currentAddressController.getCity(), 
			currentAddressController.getState(), 
			currentAddressController.getZipCode(), 
			currentAddressController.getYearsAtResidence()
		); 
	}
	
	public ArrayList<PersonalAddressController> getPreviousAddressList(){
		return previousAddressControllerList; 
	}

	@Override
	public String getErrorMessage() {
		switch (currentAddressController.getErrorMessage()){
	    	case ADDRESS: 
	    		return "Please enter a valid current address"; 
	    	case CITY: 
	    		return "Please enter a valid city under current address"; 
	    	case STATE: 
	    		return "Please enter a valid state under current address"; 
	    	case ZIP: 
	    		return "Please enter a valid zip code under current address";
	    	case YEARS_AT_RESIDENCE: 
	    		return "Please enter a valid number of years you've been at your current address"; 
			default:
				break; 
    	}
    	
    	for(int i = 0; i < previousAddressControllerList.size(); i++){
    		switch(previousAddressControllerList.get(i).getErrorMessage()){
	    		case ADDRESS: 
	    			return "Please enter a valid address at previous address position " + (i+ 1); 
	    		case CITY: 
	    			return "Please enter a valid city at previous address position " + (i+1); 
	    		case STATE: 
	    			return "Please select a state at previous address position " + (i+1); 
	    		case ZIP: 
	    			return "Please enter a valid zip code at previous address position " + (i+1); 
	    		case YEARS_AT_RESIDENCE: 
	    			return "Please enter a valid number of years you've been at previous address position " + (i+1); 
	    		default: 
	    			break; 
    		}
    	}
    	
    	if (getTotalNumberOfYears() < 3)
    		return "You have not provided at least 3 years of living history. Please list all previous addresses you've lived at within the last 3 years"; 
    	return "valid";
	}
	
	private double getTotalNumberOfYears(){
		double tempTotal = 0; 
		tempTotal += currentAddressController.getYearsAtResidence(); 
		
		for(int i = 0; i < previousAddressControllerList.size(); i++)
			tempTotal += previousAddressControllerList.get(i).getYearsAtResidence(); 
		
		return tempTotal; 
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController; 
	}

	@Override
	public void clearFields() {
		currentAddressController.clearFields();
  		
  		previousAddressContainer.getChildren().clear();
  		previousAddressControllerList.clear();		
	}
	
	public void deletePreviousAddress(PersonalAddressController previousAddress) {
		previousAddressContainer.getChildren().remove(previousAddressControllerList.indexOf(previousAddress)); 
		previousAddressControllerList.remove(previousAddress); 
	}
}
