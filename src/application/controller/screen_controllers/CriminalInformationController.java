package application.controller.screen_controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import application.controller.Interfaces.Clearable;
import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import application.controller.ui_component_controllers.AccidentController;
import application.controller.ui_component_controllers.DrivingAwardController;
import application.controller.ui_component_controllers.TrafficViolationController;
import application.model.Accident;
import application.model.CriminalHistory;
import application.model.TrafficViolation;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class CriminalInformationController implements Initializable, Navigable, Clearable{
    final ToggleGroup bondToggleGroup = new ToggleGroup();
    @FXML private RadioButton bondY;
    @FXML private RadioButton bondN;
    @FXML private DatePicker bondDate;
    @FXML private HBox bondDateContainer;
    
	final ToggleGroup crimeToggleGroup = new ToggleGroup();
    @FXML private RadioButton crimeY;
    @FXML private RadioButton crimeN;
    
    @FXML private VBox crimeDescriptionContainer;
    private Label crimeDescriptionTitle; 
    private TextArea crimeDescription; 
    
    final ToggleGroup licenseDeniedToggleGroup = new ToggleGroup();
    @FXML private RadioButton licenseDeniedY;
    @FXML private RadioButton licenseDeniedN;
    @FXML private VBox licenseDeniedDescriptionContainer;
    private Label licenseDeniedDescriptionTitle; 
    private TextArea licenseDeniedDescription; 

    @FXML private VBox trafficViolationsContainer;
    private ArrayList<TrafficViolationController> trafficViolationControllerList;

    @FXML private VBox accidentsContainer;
    private ArrayList<AccidentController> accidentControllerList; 

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		crimeDescriptionTitle = new Label("Please provide a description of your crime"); 
		crimeDescription = new TextArea(); 
		crimeDescription.setPrefWidth(800.0);
		crimeDescription.setPrefHeight(100.0);
		initToggleGroup(crimeY, crimeN, crimeToggleGroup);
		setCrimeToggleListener();
		
		licenseDeniedDescriptionTitle = new Label("Please provide a description of the denial, revocation, or suspension"); 
		licenseDeniedDescription = new TextArea(); 
		licenseDeniedDescription.setPrefWidth(800.0);
		licenseDeniedDescription.setPrefHeight(100.0);
		initToggleGroup(licenseDeniedY, licenseDeniedN, licenseDeniedToggleGroup); 
		setLicenseDeniedToggleListener(); 
		
		bondDateContainer.setVisible(false);
		initToggleGroup(bondY, bondN, bondToggleGroup);
		setBondDateToggleListener(); 

		trafficViolationControllerList = new ArrayList<TrafficViolationController>(); 
		accidentControllerList = new ArrayList<AccidentController>(); 
	}
	
	public void setTestData(){
		addTrafficViolation(null);
		trafficViolationControllerList.get(0).setTestData(); 
		
		addAccident(null);
		accidentControllerList.get(0).setTestData(); 
	}
	
    @FXML
    void addTrafficViolation(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/TrafficViolation.fxml"));
   	 	try {
   	 		trafficViolationsContainer.getChildren().add(loader.load());
   	 		TrafficViolationController temp = (TrafficViolationController) loader.getController();
   	 		temp.setParentController(this);
   	 		trafficViolationControllerList.add(temp); 
		} catch (IOException e) {
			System.out.println(e.toString());
		} 
    }

    @FXML
    void addAccident(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/Accident.fxml"));
   	 	try {
   	 		accidentsContainer.getChildren().add(loader.load());
   	 		AccidentController temp = (AccidentController) loader.getController(); 
			temp.setParentController(this);
   	 		accidentControllerList.add(temp); 
		} catch (IOException e) {
			System.out.println(e.toString());
		} 
    }
	
	@Override
	public String getErrorMessage() {
		if(((RadioButton) bondToggleGroup.getSelectedToggle()).getText().equals("Yes") && bondDate.getValue() == null)
			return "Please enter a valid date for your denied bond"; 
		if(((RadioButton) crimeToggleGroup.getSelectedToggle()).getText().equals("Yes") && crimeDescription.getText().isEmpty())
			return "Please enter a valid description for the crime(s) you've been convicted of"; 
		
		for(int i = 0; i < trafficViolationControllerList.size(); i++){
			switch (trafficViolationControllerList.get(i).getErrorMessage()){
			case COUNTY_OF_OCCURANCE: 
				return "Please enter a valid county of occurance for traffic violation at position: " + (i+1); 
			case YEAR_OF_OCCURANCE: 
				return "Please enter a valid year of occurance for traffic violation at position: " + (i+1); 
			case FUTURE_YEAR_OF_OCCURANCE: 
				return "Year of occurance cannot occur in the future. Please enter a valid date at position: " + (i+1); 
			case OFFENCE_DESCRIPTION: 
				return "Please enter a valid description for traffic violation at position: " + (i+1); 
			default: 
				break; 
			}
		}
		
		for(int i = 0; i < accidentControllerList.size(); i++){
			switch (accidentControllerList.get(i).getErrorMessage()){
			case DATE_OF_OCCURANCE: 
				return "Please enter a valid date of occurance for accident at position: " + (i+1); 
			case FUTURE_DATE: 
				return "Date of occurance cannot occur in the future. Please enter a valid date of occurance for accident at position: " + (i+1); 
			case NUMBER_OF_INJURIES: 
				return "Please enter a valid number of injuries (or 0) for accident at position: " + (i+1); 
			case NUMBER_OF_FATALITIES: 
				return "Please enter a valid number of fatalities (or 0) for accident at position: " + (i+1); 
			case NATURE_OF_ACCIDENT: 
				return "Please enter a valid description for accident at position: " + (i+1); 
			default: 
				break; 
			}
		}
		return "valid";
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		// TODO Auto-generated method stub
	}

	private void initToggleGroup(RadioButton yes, RadioButton no, ToggleGroup toggleGroup){
		yes.setToggleGroup(toggleGroup);
		no.setToggleGroup(toggleGroup);
		no.setSelected(true); 
	}
	
	private void setBondDateToggleListener(){
		bondToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
		    public void changed(ObservableValue<? extends Toggle> ov,
		        Toggle old_toggle, Toggle new_toggle) {
		    		if(((RadioButton) bondToggleGroup.getSelectedToggle()).getText().equals("Yes"))
		    			bondDateContainer.setVisible(true);
		    		else
		    			bondDateContainer.setVisible(false);
		        }
		});
	}
	
	private void setCrimeToggleListener(){
		crimeToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
		    public void changed(ObservableValue<? extends Toggle> ov,
			       Toggle old_toggle, Toggle new_toggle) {
			    		if (((RadioButton) crimeToggleGroup.getSelectedToggle()).getText().equals("Yes")){
			    			crimeDescriptionContainer.getChildren().add(crimeDescriptionTitle); 
			    			crimeDescriptionContainer.getChildren().add(crimeDescription); 
			    		}else
			    			crimeDescriptionContainer.getChildren().clear();
			        }
			});
	}
	
	private void setLicenseDeniedToggleListener(){
		licenseDeniedToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
		    public void changed(ObservableValue<? extends Toggle> ov,
			        Toggle old_toggle, Toggle new_toggle) {
		    	if (((RadioButton) licenseDeniedToggleGroup.getSelectedToggle()).getText().equals("Yes")){
		    		licenseDeniedDescriptionContainer.getChildren().add(licenseDeniedDescriptionTitle); 
		    		licenseDeniedDescriptionContainer.getChildren().add(licenseDeniedDescription); 
		    	}else
		    		licenseDeniedDescriptionContainer.getChildren().clear();
		    
		    }
		});
	}
	
	public CriminalHistory generateCriminalHistoryObject(){
		return new CriminalHistory(
				crimeY.isSelected(),
				crimeY.isSelected() ? crimeDescription.getText() : "N/A", 
				licenseDeniedY.isSelected(), 
				licenseDeniedDescription.getText(),
				generateTrafficViolationsList(), 
				generateAccidentList()
		); 
	}
	
	public boolean isBondDenied(){
		return bondY.isSelected(); 
	}
	
	public String getBondReason(){
		return bondDate.getValue().toString(); 
	}
	
	public boolean hasViolation(){
		return crimeY.isSelected();  
	}
	
	public String getViolationDescription(){
		return crimeDescription.getText(); 
	}
	
	private ArrayList<TrafficViolation> generateTrafficViolationsList(){
		ArrayList<TrafficViolation> temp = new ArrayList<TrafficViolation>(); 
		
		for (int i = 0; i < trafficViolationControllerList.size(); i++)
			temp.add(trafficViolationControllerList.get(i).generateTrafficViolationObject()); 
		
		return temp; 
	}
	
	private ArrayList<Accident> generateAccidentList(){
		ArrayList<Accident> temp = new ArrayList<Accident>(); 
		
		for (int i = 0; i < accidentControllerList.size(); i++)
			temp.add(accidentControllerList.get(i).generateAccidentObject()); 
		
		return temp; 
	}

	@Override
	public void clearFields() {
		bondN.setSelected(true);
		bondDate.setValue(null);
		
		crimeN.setSelected(true);
		crimeDescription.clear();
		
		licenseDeniedN.setSelected(true);
		licenseDeniedDescription.clear();
		
		trafficViolationsContainer.getChildren().clear();
		trafficViolationControllerList.clear(); 

		accidentsContainer.getChildren().clear();
		accidentControllerList.clear();
	}
	
	public void deleteTrafficViolation(TrafficViolationController trafficViolation) {
		trafficViolationsContainer.getChildren().remove(trafficViolationControllerList.indexOf(trafficViolation)); 
		trafficViolationControllerList.remove(trafficViolation); 
	}
	
	public void deleteAccident(AccidentController accident) {
		accidentsContainer.getChildren().remove(accidentControllerList.indexOf(accident)); 
		accidentControllerList.remove(accident); 
	}
}
