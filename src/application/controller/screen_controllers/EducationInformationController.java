package application.controller.screen_controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import application.controller.Interfaces.Clearable;
import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import application.controller.ui_component_controllers.DrivingAwardController;
import application.controller.ui_component_controllers.PersonalAddressController;
import application.controller.ui_component_controllers.SchoolAddressController;
import application.controller.ui_component_controllers.SpecialCourseController;
import application.model.Address;
import application.model.Award;
import application.model.Course;
import application.model.Education;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

public class EducationInformationController implements Initializable, Navigable, Clearable{
	private MainMenuController mainController; 
	
    @FXML private ComboBox<?> educationLevel;
    
    @FXML private SchoolAddressController schoolAddressController; 
    
    @FXML private VBox specialCoursesContainer;
    private ArrayList<SpecialCourseController> specialCourseControllerList; 
    
    @FXML private TextField schoolName;
    
    @FXML private VBox drivingAwardContainer;
    private ArrayList<DrivingAwardController> drivingAwardControllerList; 
    
    @FXML private TextField flatbedsExperience;
    @FXML private TextField busesExperience; 
    @FXML private TextField straightTruckExperience; 
    @FXML private TextField tractorExperience; 
    @FXML private TextField semitrailerExperience; 
    @FXML private TextField doubleExperience; 
    @FXML private TextField otherExperience; 

  	@Override
  	public void initialize(URL location, ResourceBundle resources) {
  		specialCourseControllerList = new ArrayList<SpecialCourseController>(); 
  		drivingAwardControllerList = new ArrayList<DrivingAwardController>(); 
  		
  		makeTextFieldNumeric(flatbedsExperience);
  		makeTextFieldNumeric(busesExperience); 
  		makeTextFieldNumeric(straightTruckExperience); 
  		makeTextFieldNumeric(tractorExperience); 
  		makeTextFieldNumeric(semitrailerExperience); 
  		makeTextFieldNumeric(doubleExperience); 
  		makeTextFieldNumeric(otherExperience); 
  	}
  	
  	public void setTestData(){
  		educationLevel.getSelectionModel().selectFirst();
  		schoolName.setText("Name of School");
  		
  		schoolAddressController.setTestData(); 
  		
  		addSpecialCourse(null); 
  		specialCourseControllerList.get(0).setTestData(); 
  		
  		addAward(null);
  		drivingAwardControllerList.get(0).setTestData(); 
  		
  		flatbedsExperience.setText("2");
  		busesExperience.setText("2");
  		straightTruckExperience.setText("2");
  		tractorExperience.setText("2");
  		semitrailerExperience.setText("2");
  		doubleExperience.setText("0");
  		otherExperience.setText("0");
  	}
  	
    @FXML
    void addSpecialCourse(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/SpecialCourse.fxml"));
   	 	try {
   	 		specialCoursesContainer.getChildren().add(loader.load());
   	 		SpecialCourseController temp = (SpecialCourseController) loader.getController(); 
   	 		temp.setParentController(this);
   	 		specialCourseControllerList.add(temp); 
		} catch (IOException e) {
			System.out.println(e.toString());
		} 
    }

    @FXML
    void addAward(ActionEvent event) {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/DrivingAward.fxml"));
   	 	try {
   	 		drivingAwardContainer.getChildren().add(loader.load());
   	 		DrivingAwardController temp = (DrivingAwardController) loader.getController(); 
   	 		temp.setParentController(this);
   	 		drivingAwardControllerList.add(temp); 
		} catch (IOException e) {
			System.out.println(e.toString());
		} 
    }
    
	@Override
	public String getErrorMessage() {
		if (educationLevel.getValue() == null)
			return "Please select a valid education level"; 
		if (schoolName.getText().isEmpty())
			return "Please enter a valid school name"; 
		
		switch (schoolAddressController.getErrorMessage()){
	    	case PHYSICAL_ADDRESS: 
	    		return "Please enter a valid address for the last school you attended"; 
	    	case CITY: 
	    		return "Please enter a valid city for the last school you attended"; 
	    	case STATE: 
	    		return "Please enter a valid state for the last school you attended"; 
	    	case ZIP: 
	    		return "Please enter a valid zip code for the last school you attended";
			default:
				break; 
		}
		
		for (int i = 0; i < specialCourseControllerList.size(); i++){
			switch(specialCourseControllerList.get(i).getErrorMessage()){
	    		case DESCRIPTION: 
	    			return "Please enter a valid descripiton for the special course at position " + (i+ 1); 
	    		case YEAR_OBTAINED: 
	    			return "Please enter a valid completion year for the special course at position " + (i+1); 
	    		case FUTURE_YEAR: 
	    			return "Future years are not allowed. Please enter a valid completion year for the special course at position " + (i+1); 
	    		default: 
	    			break; 
			}
		}
		
		for (int i = 0; i < drivingAwardControllerList.size(); i++){
			switch(drivingAwardControllerList.get(i).getErrorMessage()){
	    		case AWARD_DESCRIPTION: 
	    			return "Please enter a valid description for the driving award at position " + (i+ 1); 
	    		case DATE_COMPLETED: 
	    			return "Please enter a valid completion year for the driving award at position " + (i+1); 
	    		case FUTURE_DATE: 
	    			return "Future dates are not allowed. Please enter a valid completion year for the driving award at position " + (i+1); 
	    		case ISSUING_COMPANY: 
	    			return "Please enter a valid company for the driving award at position " + (i+1); 
	    		default: 
	    			break; 
			}
		}
		
		if(!isDrivingExperienceComplete())
			return "Please make sure that you've entered " + mainController.getUser().getYearsOfWorkHistory() + " years of work history"; 
		
		return "valid"; 
	}
	
	public Education getEducation(){
		return new Education(
					educationLevel.getValue().toString(), 
					schoolName.getText(), 
					generateCourseList(), 
					generateAwardList(), 
					schoolAddressController.generateAddressObject(), 
					flatbedsExperience.getText().isEmpty() ? "0" : flatbedsExperience.getText(), 
					busesExperience.getText().isEmpty() ? "0" : busesExperience.getText(), 
					straightTruckExperience.getText().isEmpty() ? "0" : straightTruckExperience.getText(), 
					tractorExperience.getText().isEmpty() ? "0" : tractorExperience.getText(), 
					semitrailerExperience.getText().isEmpty() ? "0" : semitrailerExperience.getText(), 
					doubleExperience.getText().isEmpty() ? "0" : doubleExperience.getText()
				); 
	}
	
	public String getFlatbedYears(){
		return flatbedsExperience.getText(); 
	}
	
	public String getBusesExperience(){
		return busesExperience.getText(); 
	}
	
	public String getStraightTruckExperience(){
		return straightTruckExperience.getText(); 
	}
	
	public String getTractorExperience(){
		return tractorExperience.getText(); 
	}
	
	public String getSemiTrailerExperience(){
		return semitrailerExperience.getText(); 
	}
	
	public String getDoubleExperience(){
		return doubleExperience.getText(); 
	}
	
	private ArrayList<Course> generateCourseList(){
		ArrayList<Course> temp = new ArrayList<Course>(); 
		for (int i = 0; i < specialCourseControllerList.size(); i ++)
			temp.add(specialCourseControllerList.get(i).generateCourseObject()); 
		return temp; 
	}
	
	private ArrayList<Award> generateAwardList(){
		ArrayList<Award> temp = new ArrayList<Award>(); 
		for (int i = 0; i < drivingAwardControllerList.size(); i++)
			temp.add(drivingAwardControllerList.get(i).generateAwardObject()); 
		return temp; 
	}
	
	private void makeTextFieldNumeric(TextField textField){
  		textField.textProperty().addListener(new ChangeListener<String>() {
  	        @Override
  	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
  	            if (!newValue.matches("\\d*")) {
  	                textField.setText(newValue.replaceAll("[^\\d]", ""));
  	            }
  	        }
  	    });
  	}
	
	private boolean isDrivingExperienceComplete(){
		int totalYearsOfExperience = 0; 
		totalYearsOfExperience += generateNumber(flatbedsExperience.getText()); 
		totalYearsOfExperience += generateNumber(busesExperience.getText());
		totalYearsOfExperience += generateNumber(straightTruckExperience.getText());
		totalYearsOfExperience += generateNumber(tractorExperience.getText());
		totalYearsOfExperience += generateNumber(semitrailerExperience.getText());
		totalYearsOfExperience += generateNumber(doubleExperience.getText());
		totalYearsOfExperience += generateNumber(otherExperience.getText());
		
		return totalYearsOfExperience == mainController.getUser().getYearsOfWorkHistory(); 
	}

	private int generateNumber(String string){
		try {
			return Integer.parseInt(string);
		} catch (NumberFormatException e) {
			return 0;
		}
	}
	
	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController; 
	}

	@Override
	public void clearFields() {
		educationLevel.setValue(null);; 
		schoolName.clear();
		
		schoolAddressController.clearFields(); 
  		
  		specialCoursesContainer.getChildren().clear();
  		specialCourseControllerList.clear();
  		
  		drivingAwardContainer.getChildren().clear(); 
  		drivingAwardControllerList.clear();
  		
  		flatbedsExperience.clear(); 
  		busesExperience.clear();
  		straightTruckExperience.clear();
  		tractorExperience.clear();
  		semitrailerExperience.clear();
  		doubleExperience.clear();
  		otherExperience.clear();
	}
	
	public void deleteDrivingAward(DrivingAwardController drivingAward) {
		drivingAwardContainer.getChildren().remove(drivingAwardControllerList.indexOf(drivingAward)); 
		drivingAwardControllerList.remove(drivingAward);
	}
	
	public void deleteSpecialCourse(SpecialCourseController specialCourse) {
		specialCoursesContainer.getChildren().remove(specialCourseControllerList.indexOf(specialCourse)); 
		specialCourseControllerList.remove(specialCourse); 
	}

}
