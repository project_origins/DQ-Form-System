package application.controller.screen_controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

public class FileUploadController implements Initializable, Navigable{	
	private MainMenuController mainController; 
	
    @FXML private ImageView frontOfLicense;
    @FXML private Button chooseFrontOfLicense;
    private String frontOfLicenseAbsolutePath; 

    @FXML private ImageView backOfLicense;
    @FXML private Button choseBackOfLicense;
    private String backOfLicenseAbsolutePath; 
    
  	@Override
  	public void initialize(URL location, ResourceBundle resources) {
  		
  	}
    
    @FXML
    void openBackOfLicenseSelector(ActionEvent event) {
    	FileChooser fc = new FileChooser(); 
    	fc.setTitle("Select back of license");
    	File selectedFile = fc.showOpenDialog(null);
    	
	    	if (selectedFile != null){
	    		Image image = new Image(selectedFile.toURI().toString()); 
	    		backOfLicense.setImage(image);
	    		
	    		backOfLicenseAbsolutePath = selectedFile.getAbsolutePath(); 
	    	}
    }

    @FXML
    void openFrontOfLicenseSelector(ActionEvent event) {
    	FileChooser fc = new FileChooser(); 
    	fc.setTitle("Select front of license");
    	File selectedFile = fc.showOpenDialog(null);
    	
    	if (selectedFile != null){
    		Image image = new Image(selectedFile.toURI().toString());
    		frontOfLicense.setImage(image);
    		frontOfLicenseAbsolutePath = selectedFile.getAbsolutePath(); 

    	}
    }

	@Override
	public String getErrorMessage() {
		if (frontOfLicense.getImage() == null)
			return "Please select a valid image for the front of the license"; 
		if (backOfLicense.getImage() == null)
			return "Please select a valid image for the back of the license"; 
		return "valid";
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController; 
	}
	
	public Image getFrontOfLicense(){
		return frontOfLicense.getImage(); 
	}
	
	public String getFrontOfLicenseAbsolutePath(){
		return frontOfLicenseAbsolutePath; 
	}
	
	public Image getBackOfLicense(){
		return backOfLicense.getImage(); 
	}
	
	public String getBackOfLicenseAbsolutePath(){
		return backOfLicenseAbsolutePath; 
	}
}
