package application.controller.screen_controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.json.JSONArray;
import org.json.JSONException;

import application.controller.Interfaces.Navigable;
import application.controller.helper_classes.JSONLoader;
import application.controller.menu_controllers.MainMenuController;
import application.model.Company;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class FormSelectionController implements Initializable, Navigable{
	
	private MainMenuController mainController; 
	
    @FXML
    private Button startButton;

    @FXML
    private ComboBox<String> companyComboBox;
    
    private HashMap <String, Company> companyMap; 

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		companyMap = new HashMap<String, Company>();  
		initializeCompanyComboBox();
		setCompanyComboBoxOnChangeListener(); 
	}
	
	public void setTestData(){
		companyComboBox.getSelectionModel().selectFirst();
	}

    @FXML
    void startForm(ActionEvent event) {
    	 
    }
    
    private void setCompanyComboBoxOnChangeListener(){
    	companyComboBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
            		mainController.showButtons();
            }    
        });
    }
    
    private void initializeCompanyComboBox(){
    	JSONLoader loader = new JSONLoader("/resources/json_files/companies.txt"); 
    	JSONArray jsonArray = loader.generateJSONArray(); 
    	
    	ObservableList<String> comboBoxData = FXCollections.observableArrayList();
    	
    	for(int i = 0; i < jsonArray.length(); i++){
    		Company tempCompany = null;
			try {
				tempCompany = new Company(
						jsonArray.getJSONObject(i).getString("name"), 
						jsonArray.getJSONObject(i).getString("physical_address"), 
						jsonArray.getJSONObject(i).getString("city"), 
						jsonArray.getJSONObject(i).getString("state"), 
						jsonArray.getJSONObject(i).getString("zip")
				);    		
				
				companyMap.put(jsonArray.getJSONObject(i).getString("name"), tempCompany); 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
 
    		comboBoxData.add(tempCompany.getName());     				
    	}
    	companyComboBox.setItems(comboBoxData); 
    }

	@Override
	public String getErrorMessage() {
		if(companyComboBox.getValue() == null){
			return "Please select a valid company"; 
		}
		return "valid"; 
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController;
	}
	
	public Company getSelectedCompany(){
		return companyMap.get(companyComboBox.getValue()) ;
	}
}