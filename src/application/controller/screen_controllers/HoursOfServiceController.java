package application.controller.screen_controllers;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.ResourceBundle;

import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import application.model.HoursOfService;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class HoursOfServiceController implements Initializable, Navigable{
	@FXML private Label dayOne;
    @FXML private Label dayTwo;
    @FXML private Label dayThree;
    @FXML private Label dayFour;
    @FXML private Label dayFive;
    @FXML private Label daySix;
    @FXML private Label daySeven;
    
    @FXML private TextField hoursOne;
    @FXML private TextField hoursTwo;
    @FXML private TextField hoursThree;
    @FXML private TextField hoursFour;
    @FXML private TextField hoursFive;
    @FXML private TextField hoursSix;
    @FXML private TextField hoursSeven;

    @FXML private Label total;

    @FXML private DatePicker date;
    @FXML private ComboBox<?> time;
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initTextFields();
		initDates(); 
		total.setText("0");
	}
	
	private void initTextFields() {
		makeTextFieldNumeric(hoursOne);
		makeTextFieldNumeric(hoursTwo);
		makeTextFieldNumeric(hoursThree);
		makeTextFieldNumeric(hoursFour);
		makeTextFieldNumeric(hoursFive);
		makeTextFieldNumeric(hoursSix);
		makeTextFieldNumeric(hoursSeven);
	}
	
	private void makeTextFieldNumeric(TextField textField){
  		textField.textProperty().addListener(new ChangeListener<String>() {
  	        @Override
  	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
  	            if (!newValue.matches("\\d*")) {
  	                textField.setText(newValue.replaceAll("[^\\d]", ""));
  	            }
  	        }
  	    });
  		
  		textField.textProperty().addListener((observable, oldValue, newValue) -> {
  		    generateTotal(); 
  		});
  	}
	
	private void initDates() {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		Calendar cal = Calendar.getInstance();
		// get starting date
		cal.add(Calendar.DAY_OF_YEAR, -6);

		dayOne.setText(sdf.format(cal.getTime()));
		
		cal.add(Calendar.DAY_OF_YEAR, 1);
		dayTwo.setText(sdf.format(cal.getTime()));
		
		cal.add(Calendar.DAY_OF_YEAR, 1);
		dayThree.setText(sdf.format(cal.getTime()));
		
		cal.add(Calendar.DAY_OF_YEAR, 1);
		dayFour.setText(sdf.format(cal.getTime()));
		
		cal.add(Calendar.DAY_OF_YEAR, 1);
		dayFive.setText(sdf.format(cal.getTime()));
		
		cal.add(Calendar.DAY_OF_YEAR, 1);
		daySix.setText(sdf.format(cal.getTime()));
		
		cal.add(Calendar.DAY_OF_YEAR, 1);
		daySeven.setText(sdf.format(cal.getTime()));
	}
	
	private void generateTotal() {
		double total = 0; 
		
		total += 
			Integer.parseInt(hoursOne.getText().isEmpty() ? "0" : hoursOne.getText()) + 
			Integer.parseInt(hoursTwo.getText().isEmpty() ? "0" : hoursTwo.getText()) + 
			Integer.parseInt(hoursThree.getText().isEmpty() ? "0" : hoursThree.getText()) + 
			Integer.parseInt(hoursFour.getText().isEmpty() ? "0" : hoursFour.getText()) + 
			Integer.parseInt(hoursFive.getText().isEmpty() ? "0" : hoursFive.getText()) + 
			Integer.parseInt(hoursSix.getText().isEmpty() ? "0" : hoursSix.getText()) + 
			Integer.parseInt(hoursSeven.getText().isEmpty() ? "0" : hoursSeven.getText()); 
		
		this.total.setText(Double.toString(total)); 
	}
	
	public HoursOfService generateHoursOfServiceObject() {
		HoursOfService hoursOfService = new HoursOfService(date.getValue().toString(), time.getValue().toString()); 
		hoursOfService.setDates(dayOne.getText(), dayTwo.getText(), dayThree.getText(), dayFour.getText(), dayFive.getText(), daySix.getText(), daySeven.getText());
		hoursOfService.setTimes(hoursOne.getText(), hoursTwo.getText(), hoursThree.getText(), hoursFour.getText(), hoursFive.getText(), hoursSix.getText(), hoursSeven.getText());
		return hoursOfService; 
	}

	@Override
	public String getErrorMessage() {
		if (hoursOne.getText().isEmpty())
			return "Please enter the number of hours worked on " + dayOne.getText() + " or the number 0 if you were not active on that day."; 
		if (hoursTwo.getText().isEmpty())
			return "Please enter the number of hours worked on " + dayTwo.getText() + " or the number 0 if you were not active on that day."; 
		if (hoursThree.getText().isEmpty())
			return "Please enter the number of hours worked on " + dayThree.getText() + " or the number 0 if you were not active on that day."; 
		if (hoursFour.getText().isEmpty())
			return "Please enter the number of hours worked on " + dayFour.getText() + " or the number 0 if you were not active on that day."; 
		if (hoursFive.getText().isEmpty())
			return "Please enter the number of hours worked on " + dayFive.getText() + " or the number 0 if you were not active on that day."; 
		if (hoursSix.getText().isEmpty())
			return "Please enter the number of hours worked on " + daySix.getText() + " or the number 0 if you were not active on that day."; 
		if (hoursSeven.getText().isEmpty())
			return "Please enter the number of hours worked on " + daySeven.getText() + " or the number 0 if you were not active on that day."; 
		if (date.getValue() == null)
			return "Please enter a valid date of relieve above"; 
		if (time.getValue() == null)
			return "Please enter a valid time of relieve above"; 
		return "valid";
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		// TODO Auto-generated method stub
	}
	
	public void setTestData() {
		hoursOne.setText("1");
		hoursTwo.setText("1");
		hoursThree.setText("1");
		hoursFour.setText("1");
		hoursFive.setText("1");
		hoursSix.setText("1");
		hoursSeven.setText("1");
		
		date.setValue(LocalDate.now());
		time.getSelectionModel().selectFirst();
	}
}
