package application.controller.screen_controllers;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;

import com.qoppa.pdf.PDFException;
import com.qoppa.pdfViewerFX.PDFViewer;

import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.HBox;

public class ReviewController implements Initializable, Navigable{
	private final String PDF_PATH = "temp_files/pdf_forms/dq_file.pdf"; 
	private PDFViewer m_PDFViewer;
	
    @FXML private HBox mainContainer;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		m_PDFViewer = new PDFViewer();
		mainContainer.getChildren().add(m_PDFViewer); 	
	}

	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return "valid";
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		// TODO Auto-generated method stub
		
	}
	
	public void setPDFPath() {
		try {
			m_PDFViewer.loadPDF(PDF_PATH);
		} catch (PDFException e) {
			e.printStackTrace();
		}
	}
}
