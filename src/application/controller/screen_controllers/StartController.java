package application.controller.screen_controllers;

import java.net.URL;
import java.util.ResourceBundle;

import application.controller.Interfaces.Navigable;
import application.controller.menu_controllers.MainMenuController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.stage.Window;

public class StartController implements Initializable, Navigable {
	private MainMenuController mainController; 
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
    @FXML
    public  void startNewDQFile(ActionEvent event) {
    		((MainMenuController) mainController).startDQFile(false); 
    }

    @FXML
    public void resumeDQFile(ActionEvent event) {
    		((MainMenuController) mainController).startDQFile(true); 
    }

    @FXML
    public void exit(ActionEvent event) {
        Platform.exit(); 
        System.exit(0);
    }

	@Override
	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParentController(MainMenuController mainController) {
		this.mainController = mainController; 
	}
}
