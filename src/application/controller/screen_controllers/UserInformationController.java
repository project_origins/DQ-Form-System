package application.controller.screen_controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import application.controller.Interfaces.Clearable;
import application.controller.Interfaces.Navigable;
import application.controller.helper_classes.TextFormatterSingleton;
import application.controller.menu_controllers.MainMenuController;
import application.controller.ui_component_controllers.EmergencyContactController;
import application.controller.ui_component_controllers.OtherDLController;
import application.controller.ui_component_controllers.RelativeController;
import application.controller.ui_component_controllers.USDLController;
import application.model.DLInfo;
import application.model.EmergencyContact;
import application.model.PersonalInformation;
import application.model.Relative;
import application.model.enums.EmploymentField;
import application.model.enums.InformationField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

public class UserInformationController implements Initializable, Navigable, Clearable{
	private final int MAX_NUMBER_OF_RELATIVES = 3;
	final ToggleGroup dlToggleGroup = new ToggleGroup();

    @FXML private TextField firstName;
    @FXML private TextField middleInitial;
	@FXML private TextField lastName;
    @FXML private TextField phoneNumber;
    @FXML private DatePicker dob;

    @FXML private TextField ssn;
    @FXML private CheckBox ssnAvailability;

    @FXML private RadioButton dlUS;
    @FXML private RadioButton dlOther;

    @FXML private TextField position;
    @FXML private TextField rateOfPay;
    @FXML private TextField reference;
    @FXML private DatePicker availabilityDate;

    @FXML private VBox relatives;
    @FXML private ArrayList<RelativeController> relativeControllerList;
    @FXML private Button addRelativesButton;

    @FXML private Parent usDLContainer;
    @FXML private USDLController usDLContainerController;

    @FXML private Parent otherDLContainer;
    @FXML private OtherDLController otherDLContainerController;

    @FXML private VBox emergencyContactContainer;

    @FXML private Button emergencyContactButton;
    private ArrayList<EmergencyContactController> emergencyContactControllerList;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initDLToggleGroup();

		relativeControllerList = new ArrayList<RelativeController>();
		emergencyContactControllerList = new ArrayList<EmergencyContactController>();

		rateOfPay.setTextFormatter(new TextFormatter<>(TextFormatterSingleton.getInstance().getDoubleFilter()));

		addEmergencyContact(null);
	}

	public void setTestData(){
		firstName.setText("Gabriel");
		middleInitial.setText("G");
		lastName.setText("Higareda");
		phoneNumber.setText("(915) 202 - 8177");
		dob.setValue(LocalDate.now());
		ssn.setText("123-41-2341");

		usDLContainerController.setTestData();

		position.setText("Position Name");
		rateOfPay.setText("28.80");
		reference.setText("Reference Name");
		availabilityDate.setValue(LocalDate.now());

		addRelative(null);
		relativeControllerList.get(0).setTestData();

		emergencyContactControllerList.get(0).setTestData();
	}

	private void initDLToggleGroup(){
		dlUS.setToggleGroup(dlToggleGroup);
		dlUS.setSelected(true);

		dlOther.setToggleGroup(dlToggleGroup);

		setDLGroupOnChangeListener();
		setSSNAvailabilityChangeListener();
	}

	private void setDLGroupOnChangeListener(){
		dlToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
		    public void changed(ObservableValue<? extends Toggle> ov,
		        Toggle old_toggle, Toggle new_toggle) {
		    		if(((RadioButton) dlToggleGroup.getSelectedToggle()).getText().equals("U.S. Issued")){
		    			usDLContainer.setVisible(true);
		    			otherDLContainer.setVisible(false);
		    		} else{
		    			usDLContainer.setVisible(false);
		    			otherDLContainer.setVisible(true);
		    		}
		        }
		});
	}

	private void setSSNAvailabilityChangeListener(){
		ssnAvailability.selectedProperty().addListener(new ChangeListener<Boolean>() {
	        public void changed(ObservableValue<? extends Boolean> ov,
	            Boolean old_val, Boolean new_val) {
	        	if(new_val == true){
	        		ssn.setDisable(true);
	        		ssn.clear();
	        	}else
	        		ssn.setDisable(false);
	        }
	    });
	}

	@FXML
    void addRelative(ActionEvent event) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/Relative.fxml"));
   	 	try {
   	 		relatives.getChildren().add(loader.load());
   	 		RelativeController temp = (RelativeController) loader.getController();
   	 		temp.setParentController(this);
   	 		relativeControllerList.add(temp);

   	 		if(relativeControllerList.size() > MAX_NUMBER_OF_RELATIVES)
   	 			disableRelativesButton();

		} catch (IOException e) {
			System.out.println(e.toString());
		}
    }

	private void disableRelativesButton() {
		addRelativesButton.setDisable(true);
	}

	public void removeRelative(RelativeController relative) {
		relatives.getChildren().remove(relativeControllerList.indexOf(relative));
		relativeControllerList.remove(relative);
	}

	private void enableRelativesButton() {
		addRelativesButton.setDisable(false);
	}

	@FXML
	void addEmergencyContact(ActionEvent event){
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/ui_components/EmergencyContact.fxml"));

		try {
			emergencyContactContainer.getChildren().add(loader.load());

   	 		EmergencyContactController temp = (EmergencyContactController) loader.getController();
   	 		temp.setParentController(this);
   	 		emergencyContactControllerList.add(temp);
		} catch (IOException e) {
			System.out.println(e.toString());
		}

		if(emergencyContactControllerList.size() > 1)
			emergencyContactButton.setDisable(true);
	}

	public void removeEmergencyContact(EmergencyContactController emergencyContact) {
		if (emergencyContactControllerList.size() > 1) {
			emergencyContactContainer.getChildren().remove(emergencyContactControllerList.indexOf(emergencyContact));
			emergencyContactControllerList.remove(emergencyContact);
		}else
			emergencyContactControllerList.get(0).clearFields();

		emergencyContactButton.setDisable(false);
	}

    @Override
    public String getErrorMessage(){
	    	if (firstName.getText().isEmpty())
	    		return "Please enter a valid first name";
	    	if (lastName.getText().isEmpty())
	    		return "Please enter a valid last name";
	    	if (phoneNumber.getText().length() != 16 || phoneNumber.getText().contains("_"))
	    		return "Please enter a valid phone number";
	    	if (dob.getValue() == null || dob.getValue().isAfter(LocalDate.now()))
	    		return "The date of birth you entered is invalid, please try again. ";
	    	if ((ssn.getText().length() != 11 || ssn.getText().contains("_")) && !ssnAvailability.isSelected())
	    		return "Please enter a valid social security number or specify you don't have one";

	    	if (((RadioButton) dlToggleGroup.getSelectedToggle()).getText().equals("U.S. Issued"))
	    		switch(usDLContainerController.getErrorMessage()){
		    		case NUMBER:
		    			return "Please enter a valid driver license number";
		    		case STATE:
		    			return "Please select a valid driver license state of issue";
		    		case EXPIRATION:
		    			return "Please enter a valid driver licence expiration date";
		    		case DATE:
		    			return "Your driver's license is expired, please enter a valid expiration date";
		    		default:
		    			break;
		    		}
			else {
		    		switch(otherDLContainerController.getErrorMessage()){
			    		case NUMBER:
			    			return "Please enter a valid driver license number";
			    		case COUNTRY:
			    			return "Please enter valid driver license country of issue";
			    		case EXPIRATION:
			    			return "Please enter a valid driver licence expiration date";
			    		case DATE:
			    			return "Your driver's license is expired, please enter a valid expiration date";
			    		default:
			    			break;
			    	}
			}

	    	if (position.getText().isEmpty())
	    		return "Please enter a valid position";
	    	if (reference.getText().isEmpty())
	    		return "Please enter a valid reference name";
	    	if (availabilityDate.getValue() == null)
	    		return "Please enter a valid availability date";
	    	if (availabilityDate.getValue().isBefore(LocalDate.now()))
	    		return "All availability dates must be future dates. Please make sure to enter a valid availability date";

	    	for (int i = 0; i < relativeControllerList.size(); i++){
	    		switch(relativeControllerList.get(i).getErrorMesssage()){
		    		case NAME:
		    			return "Please enter a valid name for your relative at position " + (i+1);
		    		case RELATION:
		    			return "Please enter a valid relation for your relative at position " + (i+1);
					default:
						break;
	    		}
	    	}

	    	for (int i = 0; i < emergencyContactControllerList.size(); i++){
	    		switch(emergencyContactControllerList.get(i).getErrorMessage()){
		    		case NAME:
		    			return "Please enter a valid name for emergency contact at position " + (i+1);
		    		case NUMBER:
		    			return "Please enter a valid number for emergency contact at position " + (i+1);
		    		case RELATIONSHIP:
		    			return "Please enter a valid relationship for emergency contact at position " + (i+1);
					default:
						break;
	    		}
	    	}

	    	return "valid";
    }

	public PersonalInformation generatePersonalInformationData(){
		return new PersonalInformation(
				firstName.getText(),
				lastName.getText(),
				middleInitial.getText(),
				ssn.getText(),
				dob.getValue().toString(),
				phoneNumber.getText()
		);
	}

	public HashMap<EmploymentField, String> generateEmploymentInformationData(){
		HashMap<EmploymentField, String> tempMap = new HashMap<EmploymentField, String>();
		tempMap.put(EmploymentField.POSITION, position.getText());
		tempMap.put(EmploymentField.RATE_OF_PAY, rateOfPay.getText());
		tempMap.put(EmploymentField.NAME_OF_REFERENCE, reference.getText());
		tempMap.put(EmploymentField.AVAILABILITY_DATE, availabilityDate.getValue().toString());
		return tempMap;
	}

	public ArrayList<EmergencyContact> generateEmergencyContactList(){
		ArrayList<EmergencyContact> tempEmergencyList = new ArrayList<EmergencyContact>();
		for (int i = 0; i < emergencyContactControllerList.size(); i++)
			tempEmergencyList.add(new EmergencyContact(emergencyContactControllerList.get(i).getName(), emergencyContactControllerList.get(i).getPhoneNumber(), emergencyContactControllerList.get(i).getRelationship()));
		return tempEmergencyList;
	}

	public DLInfo generateDLInfoObject(){
		if (dlUS.isSelected())
			return usDLContainerController.generateDLObject();
		return otherDLContainerController.generateDLObject();
	}

	public ArrayList<Relative> generateRelativeList(){
		ArrayList<Relative> tempRelativeList = new ArrayList<Relative>();
		for (int i = 0; i < relativeControllerList.size(); i++){
			tempRelativeList.add(new Relative(
					relativeControllerList.get(i).getName(),
					relativeControllerList.get(i).getRelativeRelation()
			));
		}
		return tempRelativeList;
	}

	@Override
	public void setParentController(MainMenuController mainController) {

	}

	@Override
	public void clearFields() {
		firstName.clear();
		middleInitial.clear();
		lastName.clear();
		phoneNumber.setText("(___) ___ - ____");
		dob.setValue(null);
		ssn.setText("___-__-____");

		usDLContainerController.clearFields();
		otherDLContainerController.clearFields();

		position.clear();
		rateOfPay.clear();
		reference.clear();
		availabilityDate.setValue(null);;

		relatives.getChildren().clear();
		relativeControllerList.clear();
		enableRelativesButton();

		emergencyContactControllerList.clear();
		emergencyContactContainer.getChildren().clear();
	}
}
