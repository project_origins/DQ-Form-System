package application.controller.ui_component_controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import application.controller.screen_controllers.CriminalInformationController;
import application.controller.screen_controllers.HistoryOfEmploymentController;
import application.model.PreviousEmployer;
import application.model.enums.error_messages.EmploymentEntryErrorCode;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class EmploymentEntryController implements Initializable{
	private HistoryOfEmploymentController parentController; 
	
	final ToggleGroup employmentStatusToggleGroup = new ToggleGroup();
	final ToggleGroup motorCarrierToggleGroup = new ToggleGroup(); 
	final ToggleGroup drugTestingToggleGroup = new ToggleGroup(); 
    
    @FXML private RadioButton employedRadioButton;
    @FXML private RadioButton selfEmployedRadioButton;
    @FXML private RadioButton unemployedRadioButton;
	
    @FXML private TextField companyName;
    @FXML private TextField supervisorName;
    @FXML private TextField phoneNumber;
    
    @FXML private Parent businessAddress; 
	@FXML private BusinessAddressController businessAddressController;
	
    @FXML private TextField position;
    @FXML private DatePicker fromDate;
    @FXML private DatePicker toDate;
    
    @FXML private RadioButton regulationsY;
    @FXML private RadioButton regulationsN;

    @FXML private RadioButton drugsY;
    @FXML private RadioButton drugsN;

    @FXML private TextArea reasonForLeaving;
    
    private boolean isToDateEnabled; 
    
    @FXML private ImageView deleteButton;

	@FXML
	public void deleteRow(MouseEvent event) {
		parentController.deleteEmploymentEntry(this); 
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initToggleGroups(); 
		setEmploymentStatusGroupOnChangeListeners();
	}
	
	public void setParentController(HistoryOfEmploymentController parentController) {
		this.parentController = parentController; 
	}
	
	private void initToggleGroups(){
		employedRadioButton.setToggleGroup(employmentStatusToggleGroup); employedRadioButton.setSelected(true);
		selfEmployedRadioButton.setToggleGroup(employmentStatusToggleGroup);
		unemployedRadioButton.setToggleGroup(employmentStatusToggleGroup);

		regulationsY.setToggleGroup(motorCarrierToggleGroup); 
		regulationsN.setToggleGroup(motorCarrierToggleGroup); regulationsN.setSelected(true);
		
		drugsY.setToggleGroup(drugTestingToggleGroup);
		drugsN.setToggleGroup(drugTestingToggleGroup); drugsN.setSelected(true);
		
		isToDateEnabled = true; 
	}
	
	public void setTestData(){
		companyName.setText("Name of Company");
		supervisorName.setText("Supervisor Name");
		phoneNumber.setText("(111) 234 - 1234");
		
		businessAddressController.setTestData(); 
		
		position.setText("Position");
		fromDate.setValue(LocalDate.now().minusYears(11));
		toDate.setValue(LocalDate.now());
		
		reasonForLeaving.setText("Reason for leaving");
		
	}
	
	private void setEmploymentStatusGroupOnChangeListeners(){
		employmentStatusToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
		    public void changed(ObservableValue<? extends Toggle> ov,
		        Toggle old_toggle, Toggle new_toggle) {
		    		clearFields();
		    		if(((RadioButton) employmentStatusToggleGroup.getSelectedToggle()).getText().equals("Employed"))
		    			employedFormSetup();
		    		else if (((RadioButton) employmentStatusToggleGroup.getSelectedToggle()).getText().equals("Self Employed"))
		    			selfemployedFormSetup();
		    		else
		    			unemployedFormSetup();
		    		
		        }
		});
	}
	
	private void employedFormSetup(){
		enableAllFields(); 
		
		if(!isToDateEnabled)
			toDate.setDisable(true);
	}
	
	private void selfemployedFormSetup(){
		enableAllFields(); 
		businessAddress.setDisable(true);
		position.setDisable(true);
		
		if(!isToDateEnabled)
			toDate.setDisable(true);
		
	}
	
	private void unemployedFormSetup(){
		companyName.setDisable(true);
		supervisorName.setDisable(true);
		phoneNumber.setDisable(true);
		
		businessAddress.setDisable(true);
		
		position.setDisable(true);
		
		if(!isToDateEnabled)
			toDate.setDisable(true); 
		
		regulationsY.setDisable(true);
		regulationsN.setDisable(true);
		
		drugsY.setDisable(true);
		drugsN.setDisable(true);
		
		reasonForLeaving.setDisable(true);
	}
	
	private void enableAllFields(){
		companyName.setDisable(false);
		supervisorName.setDisable(false);
		phoneNumber.setDisable(false);

		businessAddress.setDisable(false);
		
		position.setDisable(false);
		
		fromDate.setDisable(false);
		toDate.setDisable(false);
		
		regulationsY.setDisable(false);
		regulationsN.setDisable(false);
		
		drugsY.setDisable(false);
		drugsN.setDisable(false);
		
		reasonForLeaving.setDisable(false);
	}
	
	public void setDate(LocalDate date){
		isToDateEnabled = false;
		toDate.setValue(date);
		toDate.setDisable(true);
	}
	
	public PreviousEmployer generatePreviousEmployer(){
		String status = ""; 
		if(employedRadioButton.isSelected())
			status = "Employed"; 
		if(selfEmployedRadioButton.isSelected())
			status = "Self-Employed"; 
		if(unemployedRadioButton.isSelected())
			status = "Unemployed"; 
		
		boolean isMotorCarrier = false; 
		if(regulationsY.isSelected())
			isMotorCarrier = true; 
		 
		boolean isDrugTestingRequired = false; 
		if(drugsY.isSelected())
			isDrugTestingRequired = true; 
		
		return new PreviousEmployer(companyName.getText().trim(), status, 
				companyName.getText(), 
				supervisorName.getText(), 
				phoneNumber.getText(), 
				businessAddressController.getAddress(), 
				position.getText(), 
				fromDate.getValue().toString(), 
				toDate.getValue().toString() , 
				isMotorCarrier, 
				isDrugTestingRequired); 
	}
	
	public EmploymentEntryErrorCode getErrorMessage() {
		if (companyName.getText().trim().isEmpty() && !companyName.isDisabled())
			return EmploymentEntryErrorCode.COMPANY; 
		if (supervisorName.getText().trim().isEmpty() && !supervisorName.isDisabled())
			return EmploymentEntryErrorCode.SUPERVISOR_NAME; 
		if ((phoneNumber.getText().length() != 16 || phoneNumber.getText().contains("_")) && !phoneNumber.isDisabled())
			return EmploymentEntryErrorCode.PHONE_NUMBER; 
		
		if (businessAddressController.getErrorMessage() != EmploymentEntryErrorCode.VALID)
			return businessAddressController.getErrorMessage(); 
		
		if ((position.getText().trim().isEmpty() && !position.isDisabled()))
			return EmploymentEntryErrorCode.POSITION_HELD; 
		if ((fromDate.getValue() == null && !fromDate.isDisabled()))
			return EmploymentEntryErrorCode.FROM_DATE; 
		if ((toDate.getValue() == null && !toDate.isDisabled()))
			return EmploymentEntryErrorCode.TO_DATE; 
		
		return EmploymentEntryErrorCode.VALID;
	}
	
	public LocalDate getFromDate(){
		return fromDate.getValue(); 
	}
	
	public LocalDate getToDate(){
		return toDate.getValue();
	}
	
	public void clearFields() {
		companyName.clear(); 
		supervisorName.clear();
		phoneNumber.clear(); 
		businessAddressController.clearData(); 
		position.clear(); 
		fromDate.setValue(null);
		toDate.setValue(null);
		regulationsN.setSelected(true);
		drugsN.setSelected(true);
		reasonForLeaving.clear();
	}
}
