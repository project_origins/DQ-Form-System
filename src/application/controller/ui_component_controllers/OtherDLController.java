package application.controller.ui_component_controllers;

import java.time.LocalDate;

import application.controller.Interfaces.Clearable;
import application.model.DLInfo;
import application.model.enums.DLError;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class OtherDLController implements Clearable{
    @FXML private TextField number;
    @FXML private TextField country;
    @FXML private DatePicker expirationDate;
	
	public DLError getErrorMessage(){
		if (number.getText().trim().isEmpty())
			return DLError.NUMBER; 
		if (country.getText() == null)
			return DLError.COUNTRY; 
		if (expirationDate.getValue() == null)
			return DLError.EXPIRATION; 
		if (expirationDate.getValue().isBefore(LocalDate.now()))
			return DLError.DATE; 
		return DLError.VALID; 
	}
	
	public String getDLNumber(){
		return number.getText(); 
	}
	
	public String getDLCountry(){
		return country.getText();  
	}
	
	public String getDLExpirationDate(){
		return expirationDate.getValue().toString(); 
	}
	
	public DLInfo generateDLObject(){
		DLInfo temp = new DLInfo(); 
		temp.setDLNumber(number.getText());
		temp.setDLState(country.getText());
		temp.setDLExpirationDate(expirationDate.getValue().toString());
		return temp; 
	}

	@Override
	public void clearFields() {
		number.clear(); 
		country.clear();
		expirationDate.setValue(null);
	}
}
