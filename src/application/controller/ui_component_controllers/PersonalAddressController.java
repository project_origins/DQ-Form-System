package application.controller.ui_component_controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

import application.controller.Interfaces.Clearable;
import application.controller.helper_classes.TextFormatterSingleton;
import application.controller.screen_controllers.AddressInformationController;
import application.model.enums.error_messages.AddressErrorCode;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class PersonalAddressController implements Initializable, Clearable{
	private AddressInformationController parentController; 
	
    @FXML private TextField zip;
    @FXML private TextField address;
    @FXML private TextField city;
    @FXML private ComboBox<?> state;
    @FXML private TextField yearsAtResidence; 
    @FXML private ImageView deleteButton;
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		makeTextFieldNumeric(yearsAtResidence);
	}
	
    @FXML
    public void deleteRow(MouseEvent event) {
    		parentController.deletePreviousAddress(this); 
    }

	public  AddressErrorCode getErrorMessage() {
		if (address.getText().isEmpty())
			return AddressErrorCode.ADDRESS;
		if (city.getText().isEmpty())
			return AddressErrorCode.CITY; 
		if (state.getValue() == null)
			return AddressErrorCode.STATE; 
		if (zip.getText().length() != 5 || zip.getText().contains("_"))
			return AddressErrorCode.ZIP; 
		if (yearsAtResidence.getText().trim().isEmpty()) //TODO: implement the years at residence not having a number
			return AddressErrorCode.YEARS_AT_RESIDENCE; 
		if (Double.parseDouble(yearsAtResidence.getText().trim()) < 3)
			return AddressErrorCode.PREVIOUS_ADDRESSES_NEEDED; 
		return AddressErrorCode.VALID;
	}
	
	public String getPhysicalAddress(){
		return address.getText(); 
	}

	public String getCity(){
		return city.getText(); 
	}
	
	public String getState(){
		return state.getValue().toString(); 
	}
	
	public String getZipCode(){
		return zip.getText(); 
	}
	
	public double getYearsAtResidence(){
		try{
		return Double.parseDouble(yearsAtResidence.getText().trim()); 
		}catch(Exception e){
			return 0.0; 
		}
	}
	
	public void setParent(AddressInformationController parentController) {
		this.parentController = parentController; 
	}
	
	public void showDeleteButton() {
		deleteButton.setVisible(true);
	}
	
	public void hideDeleteButton() {
		deleteButton.setVisible(false); 
	}
	
	public void setTestData(){
		address.setText("7501 Lockheed Dr - Suite D");
		city.setText("El Paso");
		state.getSelectionModel().selectFirst();
		zip.setText("79925"); 
		yearsAtResidence.setText("1.5");
	}

	@Override
	public void clearFields() {
		address.clear();
		city.clear();
		state.setValue(null);
		zip.setText("_____");
		yearsAtResidence.clear();
		
	}

	private void makeTextFieldNumeric(TextField textField){
	    textField.setTextFormatter(new TextFormatter<>(TextFormatterSingleton.getInstance().getDoubleFilter()));
  	}
}
