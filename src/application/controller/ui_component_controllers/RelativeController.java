package application.controller.ui_component_controllers;

import application.controller.screen_controllers.UserInformationController;
import application.model.enums.error_messages.RelativeErrorCode;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

public class RelativeController{
    private UserInformationController parentController; 
    
    @FXML private TextField relativeName;
    @FXML private TextField relativeRelation;
    
    @FXML
    public void deleteRow(MouseEvent event) {
    		parentController.removeRelative(this); 
    }

	public RelativeErrorCode getErrorMesssage(){
		if(relativeName.getText().isEmpty())
			return RelativeErrorCode.NAME; 
		if(relativeRelation.getText().isEmpty())
			return RelativeErrorCode.RELATION; 
		return RelativeErrorCode.VALID; 
	}
	
	public void setParentController(UserInformationController parentController) {
		this.parentController = parentController; 
	}
	
	public String getName(){
		return relativeName.getText(); 
	}
	
	public String getRelativeRelation(){
		return relativeRelation.getText(); 
	}
	
	public void setTestData(){
		relativeName.setText("Relative Name");
		relativeRelation.setText("Relative Relationship");
	}
}
