package application.controller.ui_component_controllers;

import application.controller.Interfaces.Clearable;
import application.model.Address;
import application.model.enums.error_messages.EmploymentEntryErrorCode;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class SchoolAddressController implements Clearable{
    @FXML private TextField zip;
    @FXML private TextField address;
    @FXML private TextField city;
    @FXML private ComboBox<?> state;
	
	public  EmploymentEntryErrorCode getErrorMessage() {
		if (address.getText().isEmpty() && !address.isDisabled())
			return EmploymentEntryErrorCode.PHYSICAL_ADDRESS;
		if (city.getText().isEmpty() && !city.isDisabled())
			return EmploymentEntryErrorCode.CITY; 
		if (state.getValue() == null && !state.isDisabled())
			return EmploymentEntryErrorCode.STATE; 
		if ((zip.getText().length() != 5 || zip.getText().contains("_")) && !zip.isDisabled())
			return EmploymentEntryErrorCode.ZIP; 
		return EmploymentEntryErrorCode.VALID;
	}
	
	public String getPhysicalAddress(){
		return address.getText(); 
	}

	public String getCity(){
		return city.getText(); 
	}
	
	public String getState(){
		return state.getValue().toString(); 
	}
	
	public String getZipCode(){
		return zip.getText(); 
	}
	
	public Address generateAddressObject(){
		return new Address(address.getText().toString(), city.getText().toString(), state.getValue().toString(), zip.getText().toString()); 
	}
	
	public void setTestData(){
		address.setText("15000 street of last school");
		city.setText("El Paso");
		state.getSelectionModel().selectFirst();
		zip.setText("79934");
	}

	@Override
	public void clearFields() {
		address.clear(); 
		city.clear();
		zip.setText("_____");
		state.setValue(null);
	}
}
