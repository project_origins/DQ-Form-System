package application.controller.ui_component_controllers;

import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;

import application.controller.helper_classes.TextFormatterSingleton;
import application.controller.screen_controllers.EducationInformationController;
import application.model.Course;
import application.model.enums.error_messages.SpecialCourseErrorCode;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class SpecialCourseController implements Initializable{
	private EducationInformationController parentController; 
	
    @FXML private TextField description;
    @FXML private TextField yearObtained;
    
	@FXML private ImageView deleteButton;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		TextFormatterSingleton.getInstance().setIntegerFilter(yearObtained); 
	}
	
	@FXML
	public void deleteRow(MouseEvent event) {
		parentController.deleteSpecialCourse(this); 
	}
	
	public void setParentController(EducationInformationController parentController) {
		this.parentController = parentController; 
	}
    
    public SpecialCourseErrorCode getErrorMessage(){
		if (description.getText().trim().isEmpty())
			return SpecialCourseErrorCode.DESCRIPTION; 
		
		if (yearObtained.getText().trim().isEmpty())
			return SpecialCourseErrorCode.YEAR_OBTAINED; 
		
		if (Integer.parseInt(yearObtained.getText()) > Calendar.getInstance().get(Calendar.YEAR))
			return SpecialCourseErrorCode.FUTURE_YEAR; 
		
		return SpecialCourseErrorCode.VALID; 
	}
	
    public String getDescription(){
    	return description.getText().trim(); 
    }
    
    public String getYearObtained(){
    	return yearObtained.getText().trim(); 
    }
    
    public Course generateCourseObject(){
    	return new Course(description.getText(), yearObtained.getText()); 
    }
    
    public void setTestData(){
    	description.setText("Description of Special Course");
    	yearObtained.setText("1998");
    }
}
