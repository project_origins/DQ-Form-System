package application.controller.ui_component_controllers;

import java.net.URL;
import java.util.Calendar;
import java.util.ResourceBundle;

import application.controller.screen_controllers.CriminalInformationController;
import application.controller.screen_controllers.EducationInformationController;
import application.model.TrafficViolation;
import application.model.enums.error_messages.TrafficViolationError;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class TrafficViolationController implements Initializable{
	private CriminalInformationController parentController; 
	
    @FXML private TextField yearOfOccurance;
    @FXML private TextField countyOfOccurance;
    @FXML private TextArea offenseDescription;
    
	@FXML private ImageView deleteButton;
	
	@FXML
	public void deleteRow(MouseEvent event) {
		parentController.deleteTrafficViolation(this);  
	}
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Implement year of occurrence to only allow numbers
	}
	
	
	public void setParentController(CriminalInformationController parentController) {
		this.parentController = parentController; 
	}
    
    public TrafficViolationError getErrorMessage(){
	    	if (yearOfOccurance.getText().trim().isEmpty())
	    		return TrafficViolationError.YEAR_OF_OCCURANCE; 
	    	if (Integer.parseInt(yearOfOccurance.getText().toString()) > Calendar.getInstance().get(Calendar.YEAR))
	    		return TrafficViolationError.FUTURE_YEAR_OF_OCCURANCE; 
	    	if (countyOfOccurance.getText().trim().isEmpty())
	    		return TrafficViolationError.COUNTY_OF_OCCURANCE; 
	    	if (offenseDescription.getText().trim().isEmpty())
	    		return TrafficViolationError.OFFENCE_DESCRIPTION; 
    	return TrafficViolationError.VALID; 
	}

	public String getYearOfOccurance(){
		return yearOfOccurance.getText().trim(); 
    }
	  
	public String getCountyOfOccurance(){
		return countyOfOccurance.getText().trim(); 
	}
	
	public String getOffenseDescription(){
		return offenseDescription.getText().trim(); 
	}
	
	public TrafficViolation generateTrafficViolationObject(){
		return new TrafficViolation(yearOfOccurance.getText(), countyOfOccurance.getText(), offenseDescription.getText()); 
	}
	
	public void setTestData(){
		countyOfOccurance.setText("El Paso");
		yearOfOccurance.setText("2008");
		offenseDescription.setText("Description of offense");
	}
}
