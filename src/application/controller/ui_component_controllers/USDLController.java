package application.controller.ui_component_controllers;

import java.time.LocalDate;

import application.controller.Interfaces.Clearable;
import application.model.DLInfo;
import application.model.enums.DLError;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class USDLController implements Clearable{
	
    @FXML private TextField number;
    @FXML private ComboBox<?> state;
    @FXML private DatePicker expirationDate;
	
	public DLError getErrorMessage(){
		if (number.getText().trim().isEmpty())
			return DLError.NUMBER; 
		if (state.getValue() == null)
			return DLError.STATE; 
		if (expirationDate.getValue() == null)
			return DLError.EXPIRATION; 
		if (expirationDate.getValue().isBefore(LocalDate.now()))
			return DLError.DATE; 
		return DLError.VALID; 
	}
	
	public String getDLNumber(){
		return number.getText(); 
	}
	
	public String getDLState(){
		return state.getValue().toString(); 
	}
	
	public String getDLExpirationDate(){
		return expirationDate.getValue().toString(); 
	}
	
	public DLInfo generateDLObject(){
		DLInfo temp = new DLInfo(); 
		temp.setDLNumber(number.getText());
		temp.setDLState(state.getValue().toString());
		temp.setDLExpirationDate(expirationDate.getValue().toString());
		return temp; 
	}
	
	public void setTestData(){
		number.setText("21223040");
		state.getSelectionModel().selectFirst();
		expirationDate.setValue(LocalDate.now());
	}

	@Override
	public void clearFields() {
		number.clear(); 
		state.setValue(null);
		expirationDate.setValue(null);
	}
}
