package application.model;

public class Accident {
	private String dateOfOccurance; 
	private String natureOfAccident; 
	private String injuries; 
	private String fatalities;
	
	public Accident(String dateOfOccurance, String natureOfAccident, String injuries, String fatalities) {
		this.dateOfOccurance = dateOfOccurance;
		this.natureOfAccident = natureOfAccident;
		this.injuries = injuries;
		this.fatalities = fatalities;
	}

	public String getDateOfOccurance() {
		return dateOfOccurance;
	}

	public void setDateOfOccurance(String dateOfOccurance) {
		this.dateOfOccurance = dateOfOccurance;
	}

	public String getNatureOfAccident() {
		return natureOfAccident;
	}

	public void setNatureOfAccident(String natureOfAccident) {
		this.natureOfAccident = natureOfAccident;
	}

	public String getInjuries() {
		return injuries;
	}

	public void setInjuries(String injuries) {
		this.injuries = injuries;
	}

	public String getFatalities() {
		return fatalities;
	}

	public void setFatalities(String fatalities) {
		this.fatalities = fatalities;
	}
	
	public String toString(){
		return "[ " + dateOfOccurance + " - " + natureOfAccident + " - Total Injuries: " + injuries + " - Total Fatalities: " + fatalities + " ]";  
	}
}
