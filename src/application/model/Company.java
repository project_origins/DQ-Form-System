package application.model;

public class Company {
	private String name; 
	private Address address; 
	
	public Company(String name, String physicalAddress, String city, String state, String zipCode) {
		this.name = name; 
		this.address = new Address(physicalAddress, city, state, zipCode); 
	} 
	
	public String getName(){
		return name; 
	}
	
	public Address getAddress(){
		return address; 
	}
}
