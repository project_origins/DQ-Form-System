package application.model;

public class Course {
	private String name; 
	private String dateCompleted; 
	
	public Course (String name, String dateCompleted){
		this.name = name; 
		this.dateCompleted = dateCompleted; 
	}
	
	public String toString(){
		return "[ "+name+" - "+dateCompleted+" ]"; 
	}
}
