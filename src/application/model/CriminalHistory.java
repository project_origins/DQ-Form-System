package application.model;

import java.util.ArrayList;

public class CriminalHistory {
	private boolean hasCrimeConviction; 
	private String crimeExplanation; 
	
	private boolean hasSuspension; 
	private String suspensionDescription; 
	
	private ArrayList<TrafficViolation> trafficViolationList; 
	private ArrayList<Accident> accidentList;
	
	
	public CriminalHistory(boolean hasCrimeConviction, String crimeExplanation, boolean hasSuspension,
			String suspensionDescription, ArrayList<TrafficViolation> trafficViolationList, ArrayList<Accident> accidentList) {
		this.hasCrimeConviction = hasCrimeConviction;
		this.crimeExplanation = crimeExplanation;
		this.hasSuspension = hasSuspension;
		this.suspensionDescription = suspensionDescription; 
		this.trafficViolationList = trafficViolationList;
		this.accidentList = accidentList;
	}
	
	public boolean isHasCrimeConviction() {
		return hasCrimeConviction;
	}
	
	public void setHasCrimeConviction(boolean hasCrimeConviction) {
		this.hasCrimeConviction = hasCrimeConviction;
	}
	
	public String getCrimeExplanation() {
		return crimeExplanation;
	}
	
	public void setCrimeExplanation(String crimeExplanation) {
		this.crimeExplanation = crimeExplanation;
	}
	
	public boolean hasSuspension() {
		return hasSuspension;
	}
	
	public void setHasSuspension(boolean hasSuspension) {
		this.hasSuspension = hasSuspension;
	}
	
	public String getSuspensionDescription(){
		return suspensionDescription; 
	}
	
	public ArrayList<TrafficViolation> getTrafficViolationList() {
		return trafficViolationList;
	}
	
	public void setTrafficViolationList(ArrayList<TrafficViolation> trafficViolationList) {
		this.trafficViolationList = trafficViolationList;
	}
	
	public ArrayList<Accident> getAccidentList() {
		return accidentList;
	}
	
	public void setAccidentList(ArrayList<Accident> accidentList) {
		this.accidentList = accidentList;
	} 
}
