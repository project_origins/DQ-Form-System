package application.model;

import javafx.scene.image.Image;

public class DLInfo {
	private String dlNumber; 
	private String dlState; 
	private String dlExpirationDate; 
	
	private Image frontOfLicense; 
	private String frontOfLicenseAbsolutePath; 
	
	private Image backOfLicense; 
	private String backOfLicenseAbsolutePath; 
	
	public DLInfo(){
		
	}
	
	public void setDLNumber(String dlNumber){
		this.dlNumber = dlNumber; 
	}
	
	public void setDLState(String dlState){
		this.dlState = dlState; 
	}
	
	public void setDLExpirationDate(String dlExpirationDate){
		this.dlExpirationDate = dlExpirationDate; 
	}
	
	public void setFrontOfLicense(Image frontOfLicense){
		this.frontOfLicense = frontOfLicense; 
	}
	
	public void setBackOfLicense(Image backOfLicense){
		this.backOfLicense = backOfLicense; 
	}
	
	public void setFrontOfLicenseAbsolutePath(String frontOfLicenseAbsolutePath){
		this.frontOfLicenseAbsolutePath = frontOfLicenseAbsolutePath; 
	}
	
	public void setBackOfLicenseAbsolutePath(String backOfLicenseAbsolutePath){
		this.backOfLicenseAbsolutePath = backOfLicenseAbsolutePath; 
	}
	
	public String getdlNumber(){
		return dlNumber; 
	}
	
	public String getdlState(){
		return dlState; 
	}
	
	public String getdlExpirationDate(){
		return dlExpirationDate; 
	}
	
	public Image getFrontOfLicense(){
		return frontOfLicense; 
	}
	
	public Image getBackOfLicense(){
		return backOfLicense; 
	}
	
	public String getFrontOfLicenseAbsolutePath(){
		return frontOfLicenseAbsolutePath; 
	}
	
	public String getBackOfLicenseAbsolutePath(){
		return backOfLicenseAbsolutePath; 
	}
}
