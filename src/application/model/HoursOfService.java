package application.model;

public class HoursOfService {
	private String dayOne; 
	private String dayTwo; 
	private String dayThree; 
	private String dayFour; 
	private String dayFive; 
	private String daySix; 
	private String daySeven; 
	
	private String timeOne; 
	private String timeTwo; 
	private String timeThree; 
	private String timeFour; 
	private String timeFive; 
	private String timeSix; 
	private String timeSeven; 
	
	private String relievedDate; 
	private String relievedTime; 
	
	private double totalTime; 
	
	public HoursOfService(String relievedDate, String relievedTime) {
		totalTime = 0; 
		this.relievedDate = relievedDate; 
		this.relievedTime = relievedTime; 
	}
	
	public void setDates(String dayOne, String dayTwo, String dayThree, String dayFour,
			String dayFive, String daySix, String daySeven) {
		this.dayOne = dayOne; 
		this.dayTwo = dayTwo; 
		this.dayThree = dayThree; 
		this.dayFour = dayFour; 
		this.dayFive = dayFive; 
		this.daySix = daySix; 
		this.daySeven = daySeven; 
	}
	
	public void setTimes(String timeOne, String timeTwo, String timeThree, String timeFour, 
			String timeFive, String timeSix, String timeSeven) {
		this.timeOne = timeOne; 
		this.timeTwo = timeTwo; 
		this.timeThree = timeThree; 
		this.timeFour = timeFour; 
		this.timeFive = timeFive; 
		this.timeSix = timeSix; 
		this.timeSeven = timeSeven; 
	}
	
	public String generateTotalTimeString() {
		int total = 0; 
		
		total += 
			Integer.parseInt(timeOne) +
			Integer.parseInt(timeTwo) +
			Integer.parseInt(timeThree) +
			Integer.parseInt(timeFour) +
			Integer.parseInt(timeFive) +
			Integer.parseInt(timeSix) +
			Integer.parseInt(timeSeven) 
		; 
		
		return Integer.toString(total); 
	}

	public String getDayOne() {
		return dayOne;
	}

	public void setDayOne(String dayOne) {
		this.dayOne = dayOne;
	}

	public String getDayTwo() {
		return dayTwo;
	}

	public void setDayTwo(String dayTwo) {
		this.dayTwo = dayTwo;
	}

	public String getDayThree() {
		return dayThree;
	}

	public void setDayThree(String dayThree) {
		this.dayThree = dayThree;
	}

	public String getDayFour() {
		return dayFour;
	}

	public void setDayFour(String dayFour) {
		this.dayFour = dayFour;
	}

	public String getDayFive() {
		return dayFive;
	}

	public void setDayFive(String dayFive) {
		this.dayFive = dayFive;
	}

	public String getDaySix() {
		return daySix;
	}

	public void setDaySix(String daySix) {
		this.daySix = daySix;
	}

	public String getDaySeven() {
		return daySeven;
	}

	public void setDaySeven(String daySeven) {
		this.daySeven = daySeven;
	}

	public String getTimeOne() {
		return timeOne;
	}

	public void setTimeOne(String timeOne) {
		this.timeOne = timeOne;
	}

	public String getTimeTwo() {
		return timeTwo;
	}

	public void setTimeTwo(String timeTwo) {
		this.timeTwo = timeTwo;
	}

	public String getTimeThree() {
		return timeThree;
	}

	public void setTimeThree(String timeThree) {
		this.timeThree = timeThree;
	}

	public String getTimeFour() {
		return timeFour;
	}

	public void setTimeFour(String timeFour) {
		this.timeFour = timeFour;
	}

	public String getTimeFive() {
		return timeFive;
	}

	public void setTimeFive(String timeFive) {
		this.timeFive = timeFive;
	}

	public String getTimeSix() {
		return timeSix;
	}

	public void setTimeSix(String timeSix) {
		this.timeSix = timeSix;
	}

	public String getTimeSeven() {
		return timeSeven;
	}

	public void setTimeSeven(String timeSeven) {
		this.timeSeven = timeSeven;
	}

	public String getRelievedDate() {
		return relievedDate;
	}

	public void setRelievedDate(String relievedDate) {
		this.relievedDate = relievedDate;
	}

	public String getRelievedTime() {
		return relievedTime;
	}

	public void setRelievedTime(String relievedTime) {
		this.relievedTime = relievedTime;
	}
}
