package application.model;


public class PersonalInformation {
	private String firstName; 
	private String lastName; 
	private String middleInitial; 
	private String ssn; //TODO: HASH THIS VALUE
	
	private String dob; 
	private String phoneNumber; 
	

	public PersonalInformation(String firstName, String lastName, String middleInitial, String ssn, String dob,
			String phoneNumber) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleInitial = middleInitial;
		this.ssn = ssn;
		this.dob = dob;
		this.phoneNumber = phoneNumber;
	}
	
	public String getName(){
		return firstName + " " + middleInitial + " " + lastName; 
	}

	public String getFirstName() {
		return firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public String getMiddleInitial() {
		return middleInitial;
	}


	public String getSsn() {
		return ssn;
	}


	public String getDob() {
		return dob;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}
}
