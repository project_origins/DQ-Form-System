package application.model;

public class PreviousEmployer { 
	private String companyName; 
	private String status; 
	private String name; 
	private String supervisorName; 
	private String phoneNumber; 
	
	private Address address; 
	
	private String positionHeld; 
	private String fromDate; 
	private String toDate; 
	
	private boolean isMotorCarrier; 
	private boolean isDrugTestingRequired;
	
	public PreviousEmployer(String companyName, String status, String name, String supervisorName, String phoneNumber, Address address,
			String positionHeld, String fromDate, String toDate, boolean isMotorCarrier,
			boolean isDrugTestingRequired) {
		this.companyName = companyName; 
		this.status = status; 
		this.name = name;
		this.supervisorName = supervisorName;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.positionHeld = positionHeld;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.isMotorCarrier = isMotorCarrier;
		this.isDrugTestingRequired = isDrugTestingRequired;
	}  

	public String getStatus() {
		return status; 
	}
	
	public String getFromDate(){
		return fromDate; 
	}
	
	public String getToDate(){
		return toDate; 
	}
	
	public String getCompanyName(){
		return companyName; 
	}
	
	public String getSupervisorName(){
		return supervisorName; 
	}
	
	public Address getAddress(){
		return address; 
	}
	
	public String getPhoneNumber(){
		return phoneNumber; 
	}
	
	public String getPositionHeld(){
		return positionHeld; 
	}
	
	public boolean isMotorCarrier(){
		return isMotorCarrier; 
	}
	
	public boolean isDrugTestingRequired(){
		return isDrugTestingRequired; 
	}
}
