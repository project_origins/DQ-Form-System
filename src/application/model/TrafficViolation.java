package application.model;

public class TrafficViolation {
	private String yearOfOccurence; 
	private String locationOfOccurance; 
	private String offenseDescription; 
	
	public TrafficViolation (String yearOfOccurence, String locationOfOccurance, String offenseDescription){
		this.yearOfOccurence = yearOfOccurence; 
		this.locationOfOccurance = locationOfOccurance; 
		this.offenseDescription = offenseDescription; 
	}

	public String getYearOfOccurence() {
		return yearOfOccurence;
	}

	public void setYearOfOccurence(String yearOfOccurence) {
		this.yearOfOccurence = yearOfOccurence;
	}

	public String getLocationOfOccurance() {
		return locationOfOccurance;
	}

	public void setLocationOfOccurance(String locationOfOccurance) {
		this.locationOfOccurance = locationOfOccurance;
	}

	public String getOffenseDescription() {
		return offenseDescription;
	}

	public void setOffenseDescription(String offenseDescription) {
		this.offenseDescription = offenseDescription;
	}
	
	public String toString(){
		return "[ " + yearOfOccurence + " " + offenseDescription + " " + locationOfOccurance + " ]"; 
	}
}
