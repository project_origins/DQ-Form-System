package application.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import application.controller.ui_component_controllers.PersonalAddressController;
import application.model.enums.EmploymentField;
import application.model.enums.InformationField;
import javafx.scene.image.Image;

public class User {
	private Company companyApplyingFor;

	private PersonalInformation personalInformation;

	private DLInfo dlInfo;

	private Address currentAddress;
	private ArrayList<Address> previousAddressList;

	private String positionAppliedFor;
	private String rateOfPay;
	private String nameOfReference;
	private String availableDate;
	private ArrayList<Relative> employedRelativeList;

	private ArrayList<EmergencyContact> emergencyContactList;

	private ArrayList<PreviousEmployer> employmentHistory;

	private Education education;
	private CriminalHistory criminalHistory;
	private DrivingExperience drivingExperience;

	private boolean hasBondDenied;
	private String bondDeniedReason;

	private boolean hasViolation;
	private String violationReason;

	private HoursOfService hoursOfService;

	public User(){
		dlInfo = new DLInfo();
		previousAddressList = new ArrayList<Address>();
		employmentHistory = new ArrayList<PreviousEmployer>();
		emergencyContactList = new ArrayList<EmergencyContact>();
		employedRelativeList = new ArrayList<Relative>();
	}

	public void setCompanyApplyingForInformation(Company company){
		companyApplyingFor = company;
	};

	public void setPersonalInformation(PersonalInformation personalInformation){
		this.personalInformation = personalInformation;
	 }

	public void setHourseOfService(HoursOfService hoursOfService) {
		this.hoursOfService = hoursOfService;
	}

	public HoursOfService getHoursOfService() {
		return hoursOfService;
	}

    public void setUserCurrentAddress(Address currentAddress){
    		this.currentAddress = currentAddress;
    }

    public void setUserPreviousAddresses(ArrayList<PersonalAddressController> previousAddressList){
    	for (int i = 0; i < previousAddressList.size(); i++)
    		this.previousAddressList.add(new Address(
    				previousAddressList.get(i).getPhysicalAddress(),
    				previousAddressList.get(i).getCity(),
    				previousAddressList.get(i).getState(),
    				previousAddressList.get(i).getZipCode(),
    				previousAddressList.get(i).getYearsAtResidence()
    			)
    		);
    }

    public void setEmploymentHistory(ArrayList<PreviousEmployer> employmentHistory){
    	this.employmentHistory = employmentHistory;
    }

    public int getYearsOfWorkHistory(){
    		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    		Date endDate = null, startDate = null;
    		try {
    		    endDate = df.parse(employmentHistory.get(0).getToDate());
    		    startDate = df.parse(employmentHistory.get(employmentHistory.size()-1).getFromDate());

    		} catch (ParseException e) {
    		    e.printStackTrace();
    		}

    		return endDate.getYear()- startDate.getYear(); //TODO: get total years of work history
    }

    public void setEducation(Education education){
    	this.education = education;
    }

    public void setPositionInformation(HashMap<EmploymentField, String> positionData, ArrayList<Relative> employedRelativeList){
	    	positionAppliedFor = positionData.get(EmploymentField.POSITION);
	    	rateOfPay = positionData.get(EmploymentField.RATE_OF_PAY);
	    	nameOfReference = positionData.get(EmploymentField.NAME_OF_REFERENCE);
	    	availableDate = positionData.get(EmploymentField.AVAILABILITY_DATE);

	    	//TODO: add employed relative list
    }

    public void setDLInformation(String dlNumber, String dlState, String dlExpirationDate){
	    	dlInfo.setDLNumber(dlNumber);
	    	dlInfo.setDLState(dlState);
	    	dlInfo.setDLExpirationDate(dlExpirationDate);
    }

    public void setDLFrontImages(Image frontOfLicense, String frontOfLicenseAbsolutePath){
	    	dlInfo.setFrontOfLicense(frontOfLicense);
	    	dlInfo.setFrontOfLicenseAbsolutePath(frontOfLicenseAbsolutePath);
    }

    public void setDLBackImage(Image backOfLicense, String backOfLicenseAbsolutePath){
	    	dlInfo.setBackOfLicense(backOfLicense);
	    	dlInfo.setBackOfLicenseAbsolutePath(backOfLicenseAbsolutePath);
    }

    public void setEmergencyContacts(ArrayList<EmergencyContact> emergencyContactList){
    		this.emergencyContactList = emergencyContactList;
    }

    public void setRelativeList(ArrayList<Relative> employedRelativeList){
    		this.employedRelativeList = employedRelativeList;
    }

    public Company getCompany(){
    		return companyApplyingFor;
    }

    public PersonalInformation getPersonalInformation(){
    		return personalInformation;
    }

    public Address getCurrentAddress(){
    		return currentAddress;
    }

    public String getYearsAtCurrentAddress(){
    		return Double.toString(currentAddress.getYearsAtResidence());
    }

    public String getPosition(){
    		return positionAppliedFor;
    }

    public String getRate(){
    		return rateOfPay;
    }

    public String getReferred(){
    		return nameOfReference;
    }

    public ArrayList<EmergencyContact> getEmergencyList(){
    		return emergencyContactList;
    }

    public String getAvailability(){
    		return availableDate;
    }

    public ArrayList<Address> getPreviousAddressList(){
    		return previousAddressList;
    }

    public ArrayList<PreviousEmployer> getHistoryOfEmployment(){
    		return employmentHistory;
    }

    public Education getEducation(){
    		return education;
    }

    public boolean hasBondDenied(){
    		return hasBondDenied;
    }

    public String getBondDeniedReason(){
    		return bondDeniedReason;
    }

    public void setBondDenied(boolean hasBondDenied){
    		this.hasBondDenied = hasBondDenied;
    }

    public void setBondDeniedReason(String bondDeniedReason){
    		this.bondDeniedReason = bondDeniedReason;
    }

    public void setViolation(boolean hasViolation){
    		this.hasViolation = hasViolation;
    }

    public boolean hasViolation(){
    		return hasViolation;
    }

    public void setViolationReason(String violationReason){
    		this.violationReason = violationReason;
    }

    public String getViolationReason(){
    		return violationReason;
    }

    public void setCriminalHistory(CriminalHistory criminalHistory){
    		this.criminalHistory = criminalHistory;
    }

    public CriminalHistory getCriminalHistory(){
    		return criminalHistory;
    }

    public DLInfo getDLInfo(){
    		return dlInfo;
    }

    public String getRelatives(){
    		String temp = "";

	    	if(employedRelativeList.isEmpty())
	    		return "N/A";

	    	for(int i = 0; i < employedRelativeList.size(); i++)
	    		temp = employedRelativeList.get(i).toString() + " ";

	    	return temp;
    }
}
