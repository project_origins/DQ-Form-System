package application.model.enums;

public enum DLError {
	VALID, NUMBER, STATE, COUNTRY, EXPIRATION, DATE
}
