package application.model.enums;

public enum InformationField {
	FIRST_NAME, MIDDLE_INITIAL, LAST_NAME, SSN, DOB, PHONE_NUMBER
}
