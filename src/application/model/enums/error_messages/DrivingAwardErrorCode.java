package application.model.enums.error_messages;

public enum DrivingAwardErrorCode {
	AWARD_DESCRIPTION, 
	ISSUING_COMPANY, 
	DATE_COMPLETED, 
	FUTURE_DATE, 
	VALID
}
