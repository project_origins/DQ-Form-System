package application.model.enums.error_messages;

public enum SpecialCourseErrorCode {
	VALID, 
	DESCRIPTION, 
	YEAR_OBTAINED, 
	FUTURE_YEAR
}
