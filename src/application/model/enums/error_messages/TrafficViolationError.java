package application.model.enums.error_messages;

public enum TrafficViolationError {
	YEAR_OF_OCCURANCE, 
	FUTURE_YEAR_OF_OCCURANCE,
	COUNTY_OF_OCCURANCE, 
	OFFENCE_DESCRIPTION, 
	VALID
}
