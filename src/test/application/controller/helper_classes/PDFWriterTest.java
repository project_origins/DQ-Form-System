package test.application.controller.helper_classes;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.junit.Before;
import org.junit.Test;

import application.controller.helper_classes.DateSingleton;
import application.controller.helper_classes.PDFBoxSingleton;
import application.controller.helper_classes.PDFWriter;
import application.model.Company;
import application.model.PersonalInformation;
import application.model.User;

public class PDFWriterTest {
	private final String PRE_TEMP_PATH = "/resources/temp_forms";
	private final String PRE_TEMPLATE_PATH = "/resources/forms";

	PDFWriter pdfWriter = new PDFWriter();
	User user = new User();

	@Before
	public void setPage1Info(){
		clearTempDirectory();
		pdfWriter.generateTempDirectories();
		pdfWriter.setUser(generateTestUser());
	}

	private void clearTempDirectory(){

	}

	private User generateTestUser(){
		user.setPersonalInformation(generateTestPersonalInformation());
		user.setCompanyApplyingForInformation(generateTestCompanyInformation());
		return user;
	}

	private PersonalInformation generateTestPersonalInformation(){
		return new PersonalInformation(
			"Gabriel",
			"Higareda",
			"",
			"100-10-1000",
			"10/17/1989",
			"915-123-1233"
		);
	}

	private Company generateTestCompanyInformation(){
		return new Company(
			"Test Company Name",
			"123 Fake St.",
			"El Paso",
			"TX",
			"799938"
		);
	}

	@Test
	public void isPage1FilledCorrectly() {
		String testPath = "temp_files/pdf_forms/temp_page1.pdf";

		pdfWriter.fillOutPage1();
		assertTrue(new File(testPath).exists());

		PDDocument page1 = PDFBoxSingleton.getInstance().loadPDFTemplate(new File(testPath));
		PDAcroForm acroForm = page1.getDocumentCatalog().getAcroForm();

		assertTrue(acroForm.getField("Name").getValueAsString().equals( user.getPersonalInformation().getName()));
		assertTrue(acroForm.getField("CompanyName").getValueAsString().equals(user.getCompany().getName()));

		PDFBoxSingleton.getInstance().closePDDocument(page1);
	}

	@Test
	public void isPage2FilledCorrectly(){
		String testPath = "temp_files/pdf_forms/temp_page2.pdf";

		pdfWriter.fillOutPage2();
		assertTrue(new File(testPath).exists());

		PDDocument page2 = PDFBoxSingleton.getInstance().loadPDFTemplate(new File(testPath));
		PDAcroForm acroForm = page2.getDocumentCatalog().getAcroForm();

		assertTrue(acroForm.getField("CompanyName").getValueAsString().equals(user.getCompany().getName()));

		PDFBoxSingleton.getInstance().closePDDocument(page2);
	}

	@Test
	public void isPage3FilledCorrectly(){
		String testPath = "temp_files/pdf_forms/temp_page3.pdf";

		pdfWriter.fillOutPage3();
		assertTrue(new File(testPath).exists());

		PDDocument page3 = PDFBoxSingleton.getInstance().loadPDFTemplate(new File(testPath));
		PDAcroForm acroForm = page3.getDocumentCatalog().getAcroForm();

		assertTrue(acroForm.getField("CompanyName").getValueAsString().equals(user.getCompany().getName()));

		PDFBoxSingleton.getInstance().closePDDocument(page3);
	}

	@Test
	public void isPage4FilledCorrectly(){
		String testPath = "temp_files/pdf_forms/temp_page4.pdf";

		pdfWriter.fillOutPage4();
		assertTrue(new File(testPath).exists());

		PDDocument page = PDFBoxSingleton.getInstance().loadPDFTemplate(new File(testPath));
		PDAcroForm acroForm = page.getDocumentCatalog().getAcroForm();

		assertTrue(acroForm.getField("CompanyName").getValueAsString().equals(user.getCompany().getName()));
		assertTrue(acroForm.getField("CompanyAddress1").getValueAsString().equals(user.getCompany().getAddress().getPhysicialAddressString()));
		assertTrue(acroForm.getField("CompanyAddress2").getValueAsString().equals(user.getCompany().getAddress().getCityStateZipString()));

		assertTrue(acroForm.getField("Date").getValueAsString().equals(DateSingleton.getInstance().getDate()));

		assertTrue(acroForm.getField("Name").getValueAsString().equals(user.getPersonalInformation().getName()));
		assertTrue(acroForm.getField("PhoneNumber").getValueAsString().equals(user.getPersonalInformation().getPhoneNumber()));
		assertTrue(acroForm.getField("CurrentAddress").getValueAsString().equals(user.getCurrentAddress().toString()));
		assertTrue(acroForm.getField("YearsAtCurrentAddress").getValueAsString().equals(user.getYearsAtCurrentAddress()));

		for(int i = 0; i < user.getPreviousAddressList().size(); i++){
			assertTrue(acroForm.getField("PreviousAddress"+(i+1)).getValueAsString().equals(user.getPreviousAddressList().get(i).toString()));
			assertTrue(acroForm.getField("PreviousAddressYears").getValueAsString().equals(Double.toString(user.getPreviousAddressList().get(i).getYearsAtResidence())));
		}

		if (user.getPreviousAddressList().isEmpty()){
			assertTrue(acroForm.getField("PreviousAddress1").getValueAsString().equals("N/A"));
			assertTrue(acroForm.getField("PreviousAddressYears1").getValueAsString().equals("N/A"));
			assertTrue(acroForm.getField("PreviousAddress2").getValueAsString().equals("N/A"));
			assertTrue(acroForm.getField("PreviousAddressYears2").getValueAsString().equals("N/A"));
		}

		PDFBoxSingleton.getInstance().closePDDocument(page);
	}

	@Test void isPage5FilledCorrectly(){
		String testPath = "temp_files/pdf_forms/temp_page5.pdf";

		pdfWriter.fillOutPage4();
		assertTrue(new File(testPath).exists());

		PDDocument page = PDFBoxSingleton.getInstance().loadPDFTemplate(new File(testPath));
		PDAcroForm acroForm = page.getDocumentCatalog().getAcroForm();

		assertTrue(acroForm.getField("CompanyName").getValueAsString().equals(user.getCompany().getName()));

		PDFBoxSingleton.getInstance().closePDDocument(page);
	}


}
